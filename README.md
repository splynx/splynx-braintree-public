Splynx BrainTree Recurring Billing Add-on
=======================

INSTALLATION
------------

Install base add-on:
~~~
cd /var/www/splynx/addons/
git clone git@bitbucket.org:splynx/splynx-addon-base-2.git && cd splynx-addon-base-2
composer install
~~~

Install Splynx BrainTree Recurring Billing Add-on:
~~~
cd /var/www/splynx/addons/
git clone git@bitbucket.org:splynx/splynx-braintree-public.git && cd splynx-braintree-rb
composer install
./yii install
~~~

Create symlink:
~~~
ln -s /var/www/splynx/addons/splynx-braintree-rb/web/ /var/www/splynx/web/braintree-rb
~~~

Create Nginx config file:
~~~
sudo nano /etc/nginx/sites-available/splynx-braintree-rb.addons
~~~

with following content:
~~~
location /braintree-rb
{
        try_files $uri $uri/ /braintree-rb/index.php?$args;
}
~~~

Restart Nginx:
~~~
sudo service nginx restart
~~~

Settings available in `/var/www/splynx/addons/splynx-braintree-rb/config/params.php`.

MISC
------------
You can update Splynx customers BrainTree account data via command:

`./yii tools/sync-braintree-to-splynx`

Note that customer id in BrainTree and customer id in Splynx must be equal!

CODE
------------

Code for portal:
```
{# {% if customer.billing_type == 'prepaid'  %} #}
        <div class="col-lg-4 col-md-4 braintree-block">
            <div class="panel panel-default">
                <div class="panel-heading"><strong>Add money by Braintree</strong></div>
                <div class="panel-body">
                    <form action="/braintree-rb/index" method="POST" class="form-horizontal">
                        <div class="form-group">
                            <label for="amount">Amount</label>
                            <div class="col-md-8">
                                <div class="input-group">
                                    <input type="number" step="0.01" name="amount" class="input-sm form-control" style="width:calc(100% - 60px)" placeholder="Amount" required>

                                    <div class="input-group-addon select-type" style="width: 60px">
                                        <button class="btn btn-primary" type="submit">Add</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
{# {% endif %} #}

<script>
    $(':not(.braintree-block) + .braintree-block, * > .braintree-block:first-of-type').
    each(function() {
        $(this).
        nextUntil(':not(.braintree-block)').
        addBack().
        wrapAll('<div class="row" />');
    });
</script>


```