<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Class BraintreeAsset
 *
 * @package app\assets
 */
class BraintreeAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
        'https://js.braintreegateway.com/web/3.76.2/js/client.min.js',
        'https://js.braintreegateway.com/web/3.76.2/js/three-d-secure.min.js',
        'https://js.braintreegateway.com/web/3.76.2/js/hosted-fields.min.js',
        'https://js.braintreegateway.com/web/3.76.2/js/paypal-checkout.min.js',
        'https://pay.google.com/gp/p/js/pay.js',
        'https://js.braintreegateway.com/web/3.76.2/js/google-payment.min.js',
        'js/braintree.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'yii\web\JqueryAsset',
    ];
}
