<?php

use app\assets\BraintreeAsset;
use yii\web\View;

/** @var string $token */
/** @var boolean $threeDSecureCheck */
/** @var float $amount */
/** @var boolean $fixedAmount */
/** @var float $minimumAmount */
/** @var boolean $isFee */
/** @var float $fee */
/** @var string $feeAmount */
/** @var string $currencySymbol */
/** @var string $currency */
/** @var boolean cardAlreadyAdded */
/** @var string $cardHolderName */
/** @var string $lastFourDigit */
/** @var string $expirationDate */
/** @var boolean $direct */
/** @var string $payNumber */
/** @var string $paymentType */
/** @var app\models\Customer $customer */
/** @var string $environment */
/** @var string $googleMerchantId */
/** @var boolean $buttonPayPal */
/** @var boolean $buttonGooglePay */

$name = addslashes($customer->name);

Yii::$app->view->registerJs("
    var _token = '{$token}';
    var _amount = {$amount};
    var _fixedAmount = " . ($fixedAmount ? 'true' : 'false') . ";
    var _minimumAmount = {$minimumAmount};
    var _threeDSecureCheck = " . ($threeDSecureCheck ? 'true' : 'false') . ";
    var _isFee = " . ($isFee ? 'true' : 'false') . ";
    var _fee = {$fee};
    var _currencySymbol = '{$currencySymbol}';
    var _currency = '{$currency}';
    var _email = '{$customer->getOneEmail()}';
    var _name = '{$name}';
    var _phone = '{$customer->getOnePhone()}';
    var _environment = '{$environment}';
    var _googleMerchantId = '{$googleMerchantId}';
    var _other_payment_methods = true;
    var _buttonPayPal = " . ($buttonPayPal ? 'true' : 'false') . ";
    var _buttonGooglePay = " . ($buttonGooglePay ? 'true' : 'false') . ";
", View::POS_HEAD);

BraintreeAsset::register($this);

?>

<div id="parentBlock">
    <div id="payment_div" class="splynx-holder-payments">
        <div class="payment-details">
            <?php if (isset($payNumber)) : ?>
                <h3><?php echo $payNumber; ?></h3>
            <?php endif; ?>
            <div class="form-horizontal">
                <div class="form-group">
                    <label><?= $paymentType ?></label>
                    <div class="input-holder">
                        <?php if (!$fixedAmount) : ?>
                            <input id="no_fixed_amount" class="value" type="text" name="amount"
                                   placeholder="<?= Yii::t('app', 'Amount'); ?>"
                                   value="<?= $amount; ?>" form="payment-form"
                                   style="width: 100%">
                        <?php else : ?>
                            <div class="value"><?= $currencySymbol . $amount; ?></div>
                        <?php endif; ?>
                    </div>
                </div>
                <?php if ($isFee) : ?>
                    <div class="form-group">
                        <label><?= Yii::t('app', 'Fee amount'); ?></label>
                        <div class="input-holder">
                            <div id="fee_amount" class="value"><?= $currencySymbol . $feeAmount ?></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label><?= Yii::t('app', 'Total amount'); ?></label>
                        <div class="input-holder">
                            <div id="total_amount"
                                 class="value"><?= $currencySymbol . ((float)$amount + (float)$feeAmount) ?></div>
                        </div>
                    </div>
                <?php endif; ?>

                <form method="post" id="payment-form">
                    <input type="hidden" name="pay_from_saved_card" value="yes">
                </form>
            </div>
        </div>
        <div id="result_div">
            <div id="invalid-feedback-common-pay" class="invalid-feedback"></div>
        </div>

        <div class="cards-wrapper">
            <?php if ($cardAlreadyAdded) : ?>
                <div class="card-holder">
                    <?= $this->render('_cardAlreadyExist', ['cardHolderName' => $cardHolderName,
                        'lastFourDigit' => $lastFourDigit,
                        'expirationDate' => $expirationDate,]) ?>
                    <button type="submit" id="already_set_card_pay_button" class="btn btn-success pay-button"
                            form="payment-form">
                        <?= Yii::t('app', 'Pay with saved card') ?>
                    </button>
                </div>
                <div class="divider"></div>
            <?php endif; ?>

            <div class="card-holder" id="cards-container">
                <?= $this->render('_cardNew') ?>
                <div id="result_div">
                    <div id="invalid-feedback-common-card" class="invalid-feedback"></div>
                </div>

                <div class="inputs">
                    <?php if (!isset($direct) or !$direct) : ?>
                        <div class="spl-switcher">
                            <input type="checkbox" id="save_card" form="checkout-form" name="save_card">
                            <label for="save_card">
                                <span class="spl-switcher-checkbox"></span>
                                <span class="spl-switcher-label"><?= Yii::t('app', 'Save and allow future charge'); ?></span>
                            </label>
                        </div>
                    <?php endif; ?>
                </div>

                <button id="card_save_button"
                        class="btn btn-success pay-button"><?= Yii::t('app', 'Pay with new card') ?>
                </button>
            </div>
        </div>

    </div>

    <?php if ($buttonPayPal === true || $buttonGooglePay === true) : ?>
        <div class="other-payment-wrapper">
            <h3>Other payment methods</h3>
            <div class="buttons-holder">
                <div id="paypal_button" class="pay-button-wrap pay-button"></div>
                <div id="google_pay_button" class="pay-button-wrap pay-button"></div>
            </div>
        </div>
    <?php endif; ?>

    <form id="checkout-form" method="post">
        <input type="hidden" id="payment_method_nonce" name="payment_method_nonce">
        <input type="hidden" id="cardholder_name_new" name="cardholder_name">
        <input type="hidden" id="checkout_form_amount" name="amount">
    </form>
</div>






