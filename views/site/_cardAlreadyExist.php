<?php

use yii\helpers\Html;

/** @var string $cardHolderName */
/** @var string $lastFourDigit */
/** @var string $expirationDate */

?>

<h3>Saved card</h3>
<div class="card">
    <div class="name-and-logo">
        <div class="name"><?= Html::img('@web/images/braintree-logo-white.png', ['height' => '20px']) ?></div>
    </div>
    <div class="bottom">
        <div class="form-group">
            <label><?= Yii::t('app', 'Cardholder name'); ?></label>
            <div class="cardholder-name form-control"><?= $cardHolderName ?></div>
        </div>
        <div class="form-group">
            <label><?= Yii::t('app', 'Card Number'); ?></label>
            <div class="hosted-field form-control card-number">xxxx xxxx xxxx <?= $lastFourDigit ?></div>
        </div>
        <div class="form-group two-cols">
            <div class="expiration-date-wrapper">
                <label class="expiration-date-label"><?= Yii::t('app', 'Exp to:'); ?></label>
                <div class="hosted-field form-control expiration-date"><?= $expirationDate ?></div>
            </div>
        </div>
    </div>
</div>
