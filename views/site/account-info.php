<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var View $this */
/** @var string $cardHolderName */
/** @var string $lastFourDigit */
/** @var string $expirationDate */

Yii::$app->view->registerJs("
$('#remove-card').submit(function(){
    $('#parentBlock').addClass('loading');
   
});", View::POS_LOAD);

?>
<div class="splynx-holder-payments">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="panel panel-default panel-no-border" id="parentBlock">
                <div class="panel-heading">
                    <strong><?= Yii::t('app', 'Card is added!'); ?></strong>
                    <button data-toggle="collapse" data-target="#cards-panel" class="collapse-button"></button>
                </div>
                <div class="panel-body collapse cards-panel in" id="cards-panel">
                    <div class="cards-wrapper" id="cards-container">
                        <div class="card-holder">

                            <?= $this->render('_cardAlreadyExist', [
                                'cardHolderName' => $cardHolderName,
                                'lastFourDigit' => $lastFourDigit,
                                'expirationDate' => $expirationDate,
                            ]) ?>

                            <?php $form = ActiveForm::begin([
                                'id' => 'remove-card',
                                'action' => Url::to(['site/remove-account']),
                                'options' => [
                                    'class' => ['show-loader-on-submit']
                                ],
                            ]) ?>
                            <?= Html::submitButton(
                                'Remove card',
                                [
                                    'class' => 'btn btn-danger',
                                    'data-confirm' => Yii::t('app', 'Are you sure you want to remove card?')
                                ]
                            ); ?>
                            <?php ActiveForm::end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
