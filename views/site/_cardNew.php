<?php

use yii\helpers\Html;

?>

<h3>New card</h3>
<div class="card">
    <div class="name-and-logo">
        <div class="name"><?= Html::img('@web/images/braintree-logo-white.png', ['height' => '20px']) ?></div>
        <div class="logo"><?= Html::img('@web/img/unknown.svg', ['id' => 'card_type_image', 'width' => '40px', 'class' => 'pull-right']) ?></div>
    </div>
    <div class="bottom">
        <div class="form-group">
            <label for="cardholder_name"><?= Yii::t('app', 'Cardholder name'); ?></label>
            <input id="cardholder_name" class="cardholder-name form-control"
                   placeholder="<?= Yii::t('app', 'Cardholder name'); ?>">
        </div>
        <div class="form-group">
            <label for="card-number"><?= Yii::t('app', 'Card Number'); ?></label>
            <div id="card-number" class="hosted-field form-control card-number"></div>
        </div>

        <div class="form-group two-cols">
            <div class="expiration-date-wrapper">
                <label for="expiration-date" class="expiration-date-label"><?= Yii::t('app', 'Exp to:'); ?></label>
                <div id="expiration-date" class="hosted-field form-control expiration-date"></div>
            </div>
        </div>
    </div>
</div>
