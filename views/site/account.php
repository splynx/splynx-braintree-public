<?php

use app\assets\BraintreeAsset;
use yii\web\View;

/** @var string $token */
/** @var boolean $threeDSecureCheck */
/** @var app\models\Customer $customer */

$name = addslashes($customer->name);

Yii::$app->view->registerJs("
    var _token = '{$token}';
    var _amount = 1;
    var _threeDSecureCheck = " . ($threeDSecureCheck ? 'true' : 'false') . ";
    var _fixedAmount = 'true'; 
    var _minimumAmount = 0;
    var _isFee = 'false';
    var _fee = 0;
    var _email = '{$customer->getOneEmail()}';
    var _name = '{$name}';
    var _phone = '{$customer->getOnePhone()}';
    var _other_payment_methods = false;
    var _buttonPayPal = 'false';
    var _buttonGooglePay = 'false';
", View::POS_HEAD);

BraintreeAsset::register($this);

?>

<div class="splynx-holder-payments">
    <div class="row" id="panel-row">
        <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="panel panel-default panel-no-border" id="parentBlock">
                <div class="panel-heading">
                    <strong><?= Yii::t('app', 'Add card'); ?></strong>
                    <button data-toggle="collapse" data-target="#cards-panel" class="collapse-button"></button>
                </div>
                <div class="panel-body collapse cards-panel in" id="cards-panel">
                    <div class="cards-wrapper" id="cards-container">
                        <div class="card-holder">
                            <?= $this->render('_cardNew') ?>

                            <div id="invalid-feedback-common-card" class="invalid-feedback"></div>
                            <button id="card_save_button" class="btn btn-success btn-mw-100">
                                <?= Yii::t('app', 'Save and allow future charge'); ?>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<form id="checkout-form" method="post">
    <input type="hidden" id="payment_method_nonce" name="payment_method_nonce">
    <input type="hidden" id="cardholder_name_new" name="cardholder_name">
</form>



