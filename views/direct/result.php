<?php

/** @var string $header */
/** @var string $message */

?>

<div class="wrapper center">
    <h1><?php echo($header) ?></h1>
    <section>
        <p><?php echo($message) ?></p>
    </section>
</div>
<?php if (isset($redirectTo)) : ?>
    <script>
        document.addEventListener('DOMContentLoaded', function () {
            setTimeout(function () {
                window.location.href = "<?= $redirectTo ?>";
            }, 3000);
        });
    </script>
<?php endif; ?>
