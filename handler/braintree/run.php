<?php

$stdin = defined('STDIN') ? STDIN : fopen('php://stdin', 'r');

echo 'Start charging..' . "\n";

// Get all data. Its is in JSON so we must decode it
$data = fgets($stdin);

// Decode JSON
$data = json_decode($data, true);
$charge_id = $data['charge_id'];
$type = isset($data['type']) ? $data['type'] : 'invoice';
$items = $data['items'];
// Use var_dump($items) to see full data

echo 'type: "' . $type . '"' . "\n";
echo 'amount items: ' . count($items) . "\n";

// Load Splynx Base Add-on vendor
require('/var/www/splynx/addons/splynx-addon-base-2/vendor/autoload.php');
require('/var/www/splynx/addons/splynx-addon-base-2/vendor/yiisoft/yii2/Yii.php');

// Load add-on vendor
require('/var/www/splynx/addons/splynx-braintree-rb/vendor/autoload.php');

$baseDir = '/var/www/splynx/addons/splynx-braintree-rb';
$configPath = $baseDir . '/config/console.php';

$application = new splynx\base\ConsoleApplication($baseDir, $configPath);

###
### You can update amount ready items using internal script
###
// 1. Get path to PHP
if (PHP_OS === 'Darwin') {
    // OS X
    $pathToPHP = '/usr/local/bin/php';
} else {
    // Linux
    $pathToPHP = '/usr/bin/php';
}
// 2. Get path to script
$pathToScript = __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR .
    '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'script' . DIRECTORY_SEPARATOR . 'tools';
// 3. Create base command
$baseCommand = $pathToPHP . ' ' . $pathToScript . ' update-charge-progress --type="' . $type . '" --id="' . $charge_id . '" --amount_ready=';
// 4. To update amount concat amount ready items to `$baseCommand` and execute it

// Start charging..
foreach ($items as $i => $item) {
    echo "\n" . date('r') . "\n";
    $application->runAction('charge/charge-' . $type, [$item['id'], $item['customer_id']]);
    echo "\n\n";

    // Update amount ready items
    $command = $baseCommand . ($i + 1);
    exec($command);
}

// If you need you can change extension of result file.
$extension = 'txt';
$command = $pathToPHP . ' ' . $pathToScript . ' change-charge-file-extension --type="' . $type . '" --id="' . $charge_id . '" --extension="' . $extension . '"';
exec($command);

echo "Finished!\n";
