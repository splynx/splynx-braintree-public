<?php

namespace app\components;

use app\models\Customer;
use Braintree\Configuration;
use ReflectionClass;
use splynx\base\ApiResponseException;
use splynx\helpers\ConfigHelper;
use Yii;
use yii\base\BaseObject;
use yii\base\UserException;
use yii\web\IdentityInterface;

class BrainTree extends BaseObject
{
    public $environment;
    public $merchantId;
    public $publicKey;
    public $privateKey;

    /**@var  Customer */
    private $_customer;

    private $_prefix = 'Braintree';


    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        /** @var Customer $customer */
        $customer = $this->getCustomer();

        ValidateConfig::checkBrainTree($customer);
        $this->setConfig();
    }

    /**
     * @return BrainTree
     */
    public static function getBraintreeComponent()
    {
        return Yii::$app->braintree;
    }

    /**
     * Set Braintree configs
     */
    private function setConfig()
    {
        ValidateConfig::checkBrainTree($this->_customer);
        $partnerId = $this->_customer === null ? null : $this->_customer->partner_id;
        $this->environment = ConfigHelper::get('environment');
        $this->merchantId = ConfigHelper::get('merchantId', $partnerId);
        $this->publicKey = ConfigHelper::get('publicKey', $partnerId);
        $this->privateKey = ConfigHelper::get('privateKey', $partnerId);

        Configuration::environment($this->environment);
        Configuration::merchantId($this->merchantId);
        Configuration::publicKey($this->publicKey);
        Configuration::privateKey($this->privateKey);
    }

    /**
     * @param string $command
     * @param string $method
     * @param mixed $values
     * @return mixed
     * @throws UserException
     */
    public function call($command, $method, $values = null)
    {
        try {
            $class = strtr('\{class}\{command}', [
                '{class}' => $this->_prefix,
                '{command}' => $command,
            ]);

            if ($values === null) {
                $values = [];
            }

            return call_user_func([$class, $method], $values);
        } catch (\Exception $e) {
            throw new UserException(Yii::t('app', 'Error: {errorMessage} \n {exceptionName} Error', [
                'errorMessage' => $e->getMessage(),
                'exceptionName' => (new ReflectionClass($e))->getShortName()
            ]));
        }
    }

    /**
     * @param string $command
     * @param string $method
     * @param mixed $values
     * @param mixed $values2
     * @return mixed
     * @throws UserException
     */
    public function multiCall($command, $method, $values, $values2 = null)
    {
        try {
            $class = strtr('{class}_{command}', [
                '{class}' => $this->_prefix,
                '{command}' => $command,
            ]);
            if ($values2) {
                return call_user_func([$class, $method], $values, $values2);
            } else {
                return call_user_func([$class, $method], $values);
            }
        } catch (\Exception $e) {
            throw new UserException(Yii::t('app', 'Error: {errorMessage} \n {exceptionName} Error', [
                'errorMessage' => $e->getMessage(),
                'exceptionName' => (new ReflectionClass($e))->getShortName()
            ]));
        }
    }

    /**
     * @param int $customer_id
     * @param array $params
     * @param string $paramName
     * @return array
     */
    public static function addMerchantId($customer_id, $params, $paramName = 'merchantAccountId')
    {
        if (ConfigHelper::get('merchant_account') and ConfigHelper::get('merchant_account_name') !== "") {
            $params[$paramName] = ConfigHelper::get('merchant_account_name');
        } else {
            // Load customer id
            $partner = (new Customer)->findById($customer_id)->partner_id;
            if (ConfigHelper::get('partners_account_name', $partner) !== "") {
                $params[$paramName] = ConfigHelper::get('partners_account_name', $partner);
            }
        }
        return $params;
    }

    /**
     * @return null|Customer|IdentityInterface
     */
    public function getCustomer()
    {
        if ($this->_customer === null) {
            try {
                /** @var Customer $customer */
                $customer = Yii::$app->user->identity;
                $this->setCustomer($customer);
                return $this->_customer;
            } catch (\Exception $e) {
                return $this->_customer;
            }
        }
        return $this->_customer;
    }

    /**
     * @param int|Customer $customer
     * @throws ApiResponseException
     */
    public function setCustomer($customer)
    {
        if ($customer instanceof Customer) {
            $this->_customer = $customer;
        } elseif (is_numeric($customer)) {
            $this->_customer = (new Customer())->findById($customer);
        }
        $this->setConfig();
    }
}
