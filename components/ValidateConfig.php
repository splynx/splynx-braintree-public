<?php

namespace app\components;

use splynx\helpers\ConfigHelper;
use splynx\v2\models\customer\BaseCustomer;
use splynx\v2\helpers\ApiHelper;
use yii;
use yii\base\Component;
use yii\base\UserException;

class ValidateConfig extends Component
{
    public static function checkAll($customer = null)
    {
        self::checkBrainTree($customer);
        self::checkApi();
    }

    /**
     * @param null|BaseCustomer $customer
     * @throws UserException
     */
    public static function checkBrainTree($customer = null)
    {
        $partnerId = $customer === null ? null : $customer->partner_id;
        if (ConfigHelper::get('merchantId', $partnerId) == '') {
            throw new UserException(Yii::t('app', 'Error: merchantId is not set. Please check your configs.'));
        }

        if (ConfigHelper::get('publicKey', $partnerId) == '') {
            throw new UserException(Yii::t('app', 'Error: publicKey is not set. Please check your configs.'));
        }

        if (ConfigHelper::get('privateKey', $partnerId) == '') {
            throw new UserException(Yii::t('app', 'Error: privateKey is not set. Please check your configs.'));
        }

        if (ConfigHelper::get('environment') == '') {
            throw new UserException(Yii::t('app', 'Error: environment is not set. Please check your configs.'));
        }
    }

    public static function checkApi()
    {
        $params = Yii::$app->params;
        if (!isset($params['api_key']) or $params['api_key'] == '') {
            throw new UserException(Yii::t('app', 'Error: api_key is not set. Please check your configs.'));
        }

        if (!isset($params['api_secret']) or $params['api_secret'] == '') {
            throw new UserException(Yii::t('app', 'Error: api_secret is not set. Please check your configs.'));
        }

        if (!isset($params['api_domain']) or $params['api_domain'] == '') {
            throw new UserException(Yii::t('app', 'Error: api_domain is not set. Please check your configs.'));
        }

        if (strpos($params['api_domain'], 'http://') !== 0 and strpos($params['api_domain'], 'https://') !== 0) {
            throw new UserException(Yii::t('app', 'Error: api_domain must start with `http://` or `https://`. Please check your configs.'));
        }

        if (substr($params['api_domain'], -1) != '/') {
            throw new UserException(Yii::t('app', 'Error: api_domain must be with last slash. Please check your configs.'));
        }

        if (!ApiHelper::checkApi()) {
            throw new UserException(Yii::t('app', 'Error: Api call error. Please check your configs.'));
        }
    }
}
