<?php

namespace app\models;

use app\components\BrainTree;
use Braintree\Transaction;
use ReflectionException;
use splynx\base\ApiResponseException;
use splynx\helpers\ConfigHelper;
use splynx\v2\models\customer\BaseCustomer;
use splynx\v2\models\finance\BankStatement;
use Yii;
use yii\base\UserException;

/**
 * Trait BrainTreeBasePaymentTrait.
 * @package app\models
 */
trait BrainTreeBasePaymentTrait
{
    use ThreeDSecureTrait;

    /** @var string */
    public $payment_method_nonce;
    /** @var string */
    public $payment_method_token;
    /** @var Transaction */
    public $transaction;

    public $result;


    private $_transactionSuccessStatuses = [
        Transaction::AUTHORIZED,
        Transaction::AUTHORIZING,
        Transaction::SETTLED,
        Transaction::SETTLING,
        Transaction::SETTLEMENT_CONFIRMED,
        Transaction::SETTLEMENT_PENDING,
        Transaction::SUBMITTED_FOR_SETTLEMENT
    ];

    /**
     * @inheritdoc
     */
    public function getAddonTitle()
    {
        return "BrainTree";
    }

    /**
     * @return bool
     */
    private function isSuccess()
    {
        return in_array($this->transaction->status, $this->_transactionSuccessStatuses);
    }

    private $_token;

    public function getToken($regenerate = false)
    {
        if ($this->_token === null or $regenerate === true) {
            $params = BrainTree::addMerchantId($this->customer_id, []);
            $this->_token = BrainTree::getBraintreeComponent()->call('ClientToken', 'generate', $params);
        }

        return $this->_token;
    }

    /**
     * @return float
     */
    public function getFormattedAmount()
    {
        return round($this->amount, 2);
    }

    /**
     * Clear other characters from the amount
     */
    public function clearCharFromAmount()
    {
        $this->amount = $this->getFormattedAmount();
    }

    /**
     * @return mixed
     * @throws UserException
     */
    public function refundBraintree()
    {
        $result = null;
        if ($this->transaction->status === 'settled' or
            $this->transaction->status === 'settling') {
            $result = BrainTree::getBraintreeComponent()->call('Transaction', 'refund', $this->transaction->id);
        } elseif ($this->transaction->status === 'authorized' or $this->transaction->status === 'submitted_for_settlement') {
            $result = BrainTree::getBraintreeComponent()->call('Transaction', 'void', $this->transaction->id);
        } else {
            $this->addError(
                'result',
                Yii::t('app', 'Braintree payment was proceeded but Splynx transaction failed. It is impossible to refund your card automatically by Braintree. Please contact us!')
            );
        }
        $this->addError(
            'result',
            Yii::t('app', 'Braintree payment was proceeded but Splynx transaction failed. Was refunded by Braintree.')
        );

        return $result;
    }

    /**
     * @return bool
     * @throws UserException
     * @throws ReflectionException
     * @throws ApiResponseException
     */
    public function checkResult()
    {
        if (!($this->bankStatement instanceof BankStatement)) {
            $this->addError('bankStatement', Yii::t('app', 'BankStatement is not set!'));
            return false;
        }
        $params = [
            'amount' => $this->getTotalAmount(),
            'orderId' => $this->bankStatement->id,
            'options' => [
                'submitForSettlement' => true
            ],
            'transactionSource' => 'unscheduled',
        ];

        if ($this->payment_method_nonce !== null) {
            $params['paymentMethodNonce'] = $this->payment_method_nonce;
        } elseif ($this->payment_method_token !== null) {
            $params['paymentMethodToken'] = $this->payment_method_token;
        }

        $params = BrainTree::addMerchantId($this->customer_id, $params);

        if (!$this->checkThreeDSecure()) {
            return false;
        }

        $result = BrainTree::getBraintreeComponent()->call('Transaction', 'sale', $params);

        if ($result->success and !is_null($result->transaction)) {
            $this->transaction = $result->transaction;
            $this->bankStatement->additional_1 = "BrainTree transaction Id: " . $this->transaction->id;
            $this->bankStatement->update();
            return true;
        } else {
            $errors = $result->errors->deepAll();
            if (!empty($errors)) {
                foreach ($errors as $error) {
                    $this->addError($error->code, $error->message);
                }
            } else {
                $this->addError('message', $result->message);
            }
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function create()
    {
        $this->clearCharFromAmount();
        $this->createBankStatement();

        if (!$this->bankStatement) {
            $this->addError('result', Yii::t('app', "Error adding bank statement!"));
            return false;
        }
        if ($this->process()) {
            return $this->isSuccess();
        } else {
            $this->error();
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function process()
    {
        if (!$this->checkResult()) {
            return false;
        }
        if (!$this->processPayment($this->transaction->id)) {
            $this->refundBraintree();
            return false;
        }
        return true;
    }


    /**
     * @return string
     */
    public function getCurrencySymbol()
    {
        if (ConfigHelper::get('merchant_account') and ConfigHelper::get('merchant_currency_symbol') !== "") {
            return ConfigHelper::get('merchant_currency_symbol');
        } else {
            // Load customer id
            /** @var BaseCustomer $customer */
            $customer = BrainTree::getBraintreeComponent()->getCustomer();
            $partner = $customer->partner_id;
            if (ConfigHelper::get('currency_symbol', $partner) !== "") {
                return ConfigHelper::get('currency_symbol', $partner);
            }
        }
        return '';
    }
}
