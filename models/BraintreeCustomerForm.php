<?php

namespace app\models;

use app\components\BrainTree;
use splynx\helpers\ConfigHelper;
use splynx\v2\models\customer\BaseCustomer;
use Yii;
use yii\base\Model;
use app\models\splynx\CustomerBilling;

class BraintreeCustomerForm extends Model
{
    use ThreeDSecureTrait;

    public $customer_id;
    public $payment_method_nonce;
    public $cardholder_name;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id', 'payment_method_nonce'], 'required'],
            [['customer_id'], 'integer'],
            [['payment_method_nonce', 'cardholder_name'], 'string']
        ];
    }

    /**
     * @return \app\components\BrainTree
     */
    private function getBrainTree()
    {
        return Yii::$app->braintree;
    }

    private $_token;

    public function getToken($regenerate = false)
    {
        if ($this->_token === null || $regenerate === true) {
            $params = BrainTree::addMerchantId($this->customer_id, []);
            $this->_token = $this->getBrainTree()->call('ClientToken', 'generate', $params);
        }

        return $this->_token;
    }

    public function addCard($id = null)
    {
        /** @var BaseCustomer $customer */
        if ($id === null) {
            $customer = Yii::$app->getUser()->getIdentity();
        } else {
            $customer = (new BaseCustomer())->findById($id);
        }

        if ($customer == null) {
            return false;
        }

        if (!$this->checkThreeDSecure()) {
            return false;
        }

        $model = PaymentAccount::get($customer->id, ConfigHelper::get('payment_account_id'));
        // Check BrainTree account
        $accountId = $model->getBrainTreeAccount();

        $result = null;
        if (!empty($accountId)) {
            // Try to find BrainTree customer
            try {
                $result = $this->getBrainTree()->call('Customer', 'find', $accountId);
            } catch (\Braintree\Exception\NotFound $e) {
            }
        }

        if ($result === null) {
            // Create account
            $emails = $customer->getEmailsArray();
            $params = [
                'firstName' => trim(substr($customer->name, 0, strpos($customer->name, ' '))),
                'lastName' => trim(substr($customer->name, strpos($customer->name, ' '))),
                'email' => reset($emails),

                'phone' => $customer->phone,
                'paymentMethodNonce' => $this->payment_method_nonce,
                'creditCard' => [
                    'cardholderName' => $this->cardholder_name,
                    'options' => BrainTree::addMerchantId($this->customer_id, [], 'verificationMerchantAccountId'),
                ],
            ];

            $result = $this->getBrainTree()->call('Customer', 'create', $params);

            if ($result->success) {
                $account = $result->customer;
            } else {
                $errors = $result->errors->deepAll();
                if (!empty($errors) && is_array($errors)) {
                    foreach ($errors as $error) {
                        $this->addError($error->code, $error->message);
                    }
                } else {
                    $this->addError('message', $result->message);
                }
                return false;
            }
        } else {
            // Create payment method
            $accountId = $result->id;

            $params = [
                'customerId' => $accountId,
                'paymentMethodNonce' => $this->payment_method_nonce,
                'cardholderName' => $this->cardholder_name,
                'options' => BrainTree::addMerchantId($this->customer_id, [], 'verificationMerchantAccountId'),
            ];

            $result = $this->getBrainTree()->call('PaymentMethod', 'create', $params);

            if ($result->success) {
                // Load customer
                $account = $this->getBrainTree()->call('Customer', 'find', $accountId);
            } else {
                $errors = $result->errors->deepAll();
                if (!empty($errors) && is_array($errors)) {
                    foreach ($errors as $error) {
                        $this->addError($error->code, $error->message);
                    }
                } else {
                    $this->addError('message', $result->message);
                }
                return false;
            }
        }

        $customerBilling = (new CustomerBilling())->findById($customer->id);
        if (!empty($customerBilling)) {
            $paymentMethodId = ConfigHelper::get('payment_method_id');
            if ($customerBilling->payment_method != $paymentMethodId) {
                $customerBilling->payment_method = $paymentMethodId;
                $customerBilling->update();
            }

            // Update payment account info
            $model = PaymentAccount::get($customer->id, ConfigHelper::get('payment_account_id'));
            $model->setBrainTreeAccount($account->id);
            $model->setPaymentToken($account->paymentMethods[0]->token);
            if (isset($account->paymentMethods[0]->cardholderName)) {
                $model->setCardHolderName($account->paymentMethods[0]->cardholderName);
            }
            if (isset($account->paymentMethods[0]->last4)) {
                $model->setCardLastFourDigit($account->paymentMethods[0]->last4);
            }
            if (isset($account->paymentMethods[0]->expirationDate)) {
                $model->setCardExpirationDate($account->paymentMethods[0]->expirationDate);
            }
            $model->update();

            return true;
        } else {
            $this->addError('message', Yii::t('app', 'Customer not found'));
            return false;
        }
    }

    public function removeCard()
    {
        $model = PaymentAccount::get($this->customer_id, ConfigHelper::get('payment_account_id'));

        $result = $this->getBrainTree()->call('PaymentMethod', 'delete', $model->getPaymentToken());

        if ($result->success) {
            // Update payment account info
            $model->setPaymentToken('');
            $model->setCardHolderName('');
            $model->setCardLastFourDigit('');
            $model->setCardExpirationDate('');
            return $model->update();
        }
        return false;
    }
}
