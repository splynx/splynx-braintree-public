<?php


namespace app\models;

use app\components\BrainTree;
use splynx\helpers\ConfigHelper;
use yii;
use yii\base\UserException;

trait ThreeDSecureTrait
{
    /**
     * Check if 3D Secure is enabled.
     * @see https://developers.braintreepayments.com/guides/3d-secure/server-side/php
     *
     * @return bool
     * @throws UserException
     */
    public function checkThreeDSecure()
    {
        if (ConfigHelper::get('threeDSecure') && $this->payment_method_nonce) {
            $paymentMethodNonce = BrainTree::getBraintreeComponent()->call('PaymentMethodNonce', 'find', $this->payment_method_nonce);

            // 3D Secure is only compatible with credit cards and non-tokenized Google Pay payment methods.
            // Any other payment methods don't require 3D Secure per PSD2 nor are they able to be passed through 3D Secure.
            if (isset($paymentMethodNonce->type) && $paymentMethodNonce->type == 'PayPalAccount') {
                return true;
            }

            if (empty($paymentMethodNonce->threeDSecureInfo)) {
                Yii::info(Yii::t('app', 'Fail to get 3D Secure info...'));
                Yii::error($paymentMethodNonce);
                $this->addError('message', Yii::t('app', 'Empty 3D Secure info'));
                return false;
            }

            if (!$paymentMethodNonce->threeDSecureInfo->liabilityShifted) {
                $this->addError('message', Yii::t('app', 'Error to check 3D Secure. Status: {status}', ['status' => $paymentMethodNonce->threeDSecureInfo->status]));
                return false;
            }

            if ($paymentMethodNonce->threeDSecureInfo->liabilityShifted && !($paymentMethodNonce->threeDSecureInfo->liabilityShiftPossible || ConfigHelper::get('threeDSecureUnSupport'))) {
                $this->addError('message', Yii::t('app', 'Error to check 3D Secure. Status: {status}', ['status' => $paymentMethodNonce->threeDSecureInfo->status]));
                return false;
            }
        }

        return true;
    }
}
