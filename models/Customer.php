<?php

namespace app\models;

use splynx\v2\models\customer\BaseCustomer;

class Customer extends BaseCustomer
{
    public function getOneEmail()
    {
        $emails = $this->getEmailsArray();
        $email = reset($emails);

        return $email;
    }

    public function getOnePhone()
    {
        $phones = $this->getPhonesArray();
        $phone = reset($phones);

        return $phone;
    }

    public function getPhonesArray()
    {
        return array_map('trim', explode(',', $this->phone));
    }
}
