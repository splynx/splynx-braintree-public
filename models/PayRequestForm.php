<?php

namespace app\models;

use splynx\v2\models\finance\payments\BasePaymentProformaInvoice;
use yii\helpers\ArrayHelper;

/**
 * Class PayRequestForm
 * @package app\models
 *
 */
class PayRequestForm extends BasePaymentProformaInvoice
{
    use BrainTreeBasePaymentTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge([
            [['payment_method_nonce', 'payment_method_token'], 'string']
        ], parent::rules());
    }
}
