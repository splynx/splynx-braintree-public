<?php

namespace app\models\splynx;

use splynx\v2\models\customer\BaseCustomerBilling;

/**
 * Class CustomerBilling
 * @package app\models\splynx
 */
class CustomerBilling extends BaseCustomerBilling
{
}
