<?php

namespace app\models;

use splynx\v2\models\finance\payments\BasePaymentInvoice;
use yii\helpers\ArrayHelper;

/**
 * Class PayInvoiceForm
 * @package app\models
 *
 */
class PayInvoiceForm extends BasePaymentInvoice
{
    use BrainTreeBasePaymentTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge([
            [['payment_method_nonce', 'payment_method_token'], 'string']
        ], parent::rules());
    }
}
