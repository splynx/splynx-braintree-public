<?php

namespace app\models;

use splynx\v2\models\customer\BasePaymentAccount;

/**
 * Class PaymentAccount.
 *
 * @property bool $isCardAlreadyAdded
 * @property string $brainTreeAccount
 * @property string $paymentToken
 * @property string $cardHolderName
 * @property int $cardLastFourDigit
 * @property string $cardExpirationDate
 *
 * @package app\models
 */
class PaymentAccount extends BasePaymentAccount
{

    private $_braintree_account_field;
    private $_payment_token;
    private $_card_4_last_digit_field;
    private $_cardholder_name_field;
    private $_card_expiration_date_field;


    public function init()
    {
        // Set fields
        $this->_braintree_account_field = 'field_1';
        $this->_payment_token = 'field_2';
        $this->_card_4_last_digit_field = 'field_3';
        $this->_cardholder_name_field = 'field_4';
        $this->_card_expiration_date_field = 'field_5';
        parent::init();
    }

    /**
     * @return bool
     */
    public function getIsCardAlreadyAdded()
    {
        return !empty($this->{$this->_payment_token});
    }

    /**
     * @return string
     */
    public function getBrainTreeAccount()
    {
        return $this->{$this->_braintree_account_field};
    }

    /**
     * @param string $value
     */
    public function setBrainTreeAccount($value)
    {
        $this->{$this->_braintree_account_field} = $value;
    }

    /**
     * @return string
     */
    public function getPaymentToken()
    {
        return $this->{$this->_payment_token};
    }

    /**
     * @param string $value
     */
    public function setPaymentToken($value)
    {
        $this->{$this->_payment_token} = $value;
    }

    /**
     * @return string
     */
    public function getCardHolderName()
    {
        return $this->{$this->_cardholder_name_field};
    }

    /**
     * @param string $value
     */
    public function setCardHolderName($value)
    {
        $this->{$this->_cardholder_name_field} = $value;
    }

    /**
     * @return int
     */
    public function getCardLastFourDigit()
    {
        return $this->{$this->_card_4_last_digit_field};
    }

    /**
     * @param int $value
     */
    public function setCardLastFourDigit($value)
    {
        $this->{$this->_card_4_last_digit_field} = $value;
    }

    /**
     * @param string $value
     */
    public function setCardExpirationDate($value)
    {
        $this->{$this->_card_expiration_date_field} = $value;
    }

    /**
     * @return string
     */
    public function getCardExpirationDate()
    {
        return $this->{$this->_card_expiration_date_field};
    }
}
