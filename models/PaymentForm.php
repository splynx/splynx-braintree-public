<?php

namespace app\models;

use splynx\helpers\ConfigHelper;
use splynx\v2\models\finance\payments\BasePaymentModel;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class PaymentForm
 * @package app\models
 *
 */
class PaymentForm extends BasePaymentModel
{
    use BrainTreeBasePaymentTrait {
        create as traitCreate;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge([
            [['payment_method_nonce', 'payment_method_token'], 'string']
        ], parent::rules());
    }

    /**
     * Create payment
     * @return bool
     */
    public function create()
    {
        if (!empty($this->getMinimumAmount()) and $this->amount < $this->getMinimumAmount()) {
            $this->addError('amount', Yii::t('app', 'Amount less than the minimum set!'));
            return false;
        }

        return $this->traitCreate();
    }

    /**
     * Get minimum amount from config
     * @return int|null
     */
    public function getMinimumAmount()
    {
        return ConfigHelper::get('minimum_amount_of_replenishment') > 0 ? ConfigHelper::get('minimum_amount_of_replenishment') : 0;
    }
}
