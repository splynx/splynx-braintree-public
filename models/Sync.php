<?php

namespace app\models;

use app\components\BrainTree;
use splynx\helpers\ConfigHelper;
use splynx\v2\models\customer\BaseCustomer;
use Yii;
use yii\base\Model;

class Sync extends Model
{
    public static $apiCall = 'admin/customers/customer';


    /**
     * sync payment method data from braintree to splynx
     */
    public function sync()
    {
        /** @var \Braintree\Customer[] $collection */
        $collection = BrainTree::getBraintreeComponent()->call('Customer', 'search', []);

        foreach ($collection as $account) {
            echo Yii::t('app', 'Work with BrainTree account #{id}', ['id' => $account->id]) . PHP_EOL;
            if (!isset($account->paymentMethods[0])) {
                echo Yii::t('app', 'BrainTree customer has no payment method!') . PHP_EOL;
                continue;
            }

            // Try to find Splynx customer
            $model = static::findPaymentAccountByAccountId($account->id);
            if ($model === null) {
                echo Yii::t('app', 'Splynx customer with such id not found!') . PHP_EOL;
                continue;
            }

            $model->setBrainTreeAccount($account->id);
            $model->setPaymentToken($account->paymentMethods[0]->token);
            if (isset($account->paymentMethods[0]->cardholderName)) {
                $model->setCardHolderName($account->paymentMethods[0]->cardholderName);
            }
            if (isset($account->paymentMethods[0]->last4)) {
                $model->setCardLastFourDigit($account->paymentMethods[0]->last4);
            }
            if (isset($account->paymentMethods[0]->expirationDate)) {
                $model->setCardExpirationDate($account->paymentMethods[0]->expirationDate);
            }

            if ($model->update()) {
                echo Yii::t('app', 'BrainTree account info saved in Splynx!');
            } else {
                echo Yii::t('app', 'BrainTree account info NOT saved in Splynx!');
            }

            echo "\n";
        }

        echo Yii::t('app', 'Finished!') . "\n";
    }

    /**
     * sync cardholder names from splynx to braintree
     */
    public function syncCardholderNameToBrainTree()
    {
        print Yii::t('app', 'Collect cardholders names in splynx') . PHP_EOL;
        //get cardholder name of existing customers by payment toket
        $cardholderNamesByTokenList = $this->getListOfPaymentAccountTokensAndCardholderNames();
        if (empty($cardholderNamesByTokenList)) {
            print Yii::t('app', 'There is no customers with BrainTree payment account in Splynx!') . PHP_EOL;
            return;
        }

        //get all customers from braintree

        /** @var \Braintree\Customer[] $brainTreeCustomers */
        $brainTreeCustomers = BrainTree::getBraintreeComponent()->call('Customer', 'search', []);
        foreach ($brainTreeCustomers as $key => $customer) {
            echo Yii::t('app', 'Work with BrainTree account #{id}', ['id' => $customer->id]) . PHP_EOL;
            //check if token exist in splynx for this customer

            if (!is_array($paymentMethods = $customer->paymentMethods)) {
                print Yii::t('app', 'BrainTree customer has no payment methods!') . PHP_EOL;
                continue;
            }

            $method = reset($paymentMethods);
            if (!isset($method->token)) {
                print Yii::t('app', 'BrainTree customer payment method token is not set!') . PHP_EOL;
                continue;
            }
            $token = $method->token;

            if (!isset($cardholderNamesByTokenList[$token])) {
                print Yii::t('app', 'There isn\'t payment account with token {token} in splynx!', ['token' => $token]) . PHP_EOL;
                continue;
            }

            //Edit cardholder name for payment method in braintree
            $params = [
                'cardholderName' => $cardholderNamesByTokenList[$token],
            ];

            $result = BrainTree::getBraintreeComponent()->multiCall('PaymentMethod', 'update', $token, $params);
            if (isset($result->success)) {
                print Yii::t('app', 'Cardholder name successfully updated!') . PHP_EOL;
            } else {
                print Yii::t('app', 'Can\'t save cardholder name to BrainTree!') . PHP_EOL;
            }
        }
        print Yii::t('app', 'Finished!') . PHP_EOL;
    }

    /**
     * get list of splynx braintree payment account tokens as key and cardholder names as value
     * @return array
     */
    public function getListOfPaymentAccountTokensAndCardholderNames()
    {
        //get all customers
        $splynxCustomers = BaseCustomer::getListAll();
        if (empty($splynxCustomers)) {
            print Yii::t('app', 'Can\'t get customers by API') . PHP_EOL;
            return [];
        }
        $cardholderNamesByTokenList = [];
        foreach ($splynxCustomers as $key => $customer) {
            //check if customer has braintree payment account
            $paymentAccount = PaymentAccount::get($customer['id'], ConfigHelper::get('payment_account_id'));
            if (!empty($paymentAccount) && !empty($paymentAccount->getPaymentToken())) {
                $cardholderNamesByTokenList[$paymentAccount->getPaymentToken()] = $paymentAccount->getCardHolderName();
            }
            unset($splynxCustomers[$key]);
        }

        return $cardholderNamesByTokenList;
    }

    /**
     * search in splynx for customer with $accountId and return his braintree payment account data
     * @param int $accountId account ID from braintree
     * @return null|PaymentAccount
     */
    public static function findPaymentAccountByAccountId($accountId)
    {
        $paymentAccount = null;
        //get all customers
        $splynxCustomers = BaseCustomer::getListAll();
        if (empty($splynxCustomers)) {
            print Yii::t('app', 'Can\'t get customers by API') . PHP_EOL;
            return $paymentAccount;
        }
        foreach ($splynxCustomers as $key => $customer) {
            $paymentAccount = PaymentAccount::get($customer->id, ConfigHelper::get('payment_account_id'));
            if (!empty($paymentAccount) && $paymentAccount->getBrainTreeAccount() == $accountId) {
                break;
            }
            unset($splynxCustomers[$key]);
        }
        return $paymentAccount;
    }
}
