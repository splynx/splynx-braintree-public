<?php

namespace app\controllers;

use app\components\ValidateConfig;
use app\models\BraintreeCustomerForm;
use app\models\PayInvoiceForm;
use app\models\PaymentAccount;
use app\models\PaymentForm;
use app\models\PayRequestForm;
use splynx\helpers\ConfigHelper;
use splynx\v2\models\finance\BaseInvoice;
use splynx\v2\models\finance\BaseProformaInvoice;
use Yii;
use yii\base\InvalidParamException;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\ErrorAction;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ],
                    [
                        'allow' => true,
                        'roles' => ['?', '@'],
                        'actions' => ['error']
                    ],
                ]
            ]
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => ErrorAction::class,
            ]
        ];
    }

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;

        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        ValidateConfig::checkBrainTree(Yii::$app->user->identity);

        $model = new PaymentForm();
        $model->customer_id = Yii::$app->getUser()->getId();

        $account = PaymentAccount::get($model->customer_id, ConfigHelper::get('payment_account_id'));

        $cardForm = new BraintreeCustomerForm();
        $cardForm->customer_id = $model->customer_id;

        if (($post = Yii::$app->getRequest()->post()) !== [] and $model->load($post, '')) {
            $paymentIsPossible = true;
            if (!empty($post['pay_from_saved_card']) and $post['pay_from_saved_card'] === 'yes') {
                $model->payment_method_token = $account->getPaymentToken();
            } elseif (!empty($post['payment_method_nonce'])) {
                $model->payment_method_nonce = $post['payment_method_nonce'];

                // Check if need save card
                if (!empty($post['save_card']) && $post['save_card'] === 'on') {
                    $cardForm->customer_id = $model->customer_id;
                    $cardForm->payment_method_nonce = $model->payment_method_nonce;
                    $cardForm->cardholder_name = trim($post['cardholder_name']);

                    // Check if card is already added
                    if ($account->isCardAlreadyAdded) {
                        $cardForm->removeCard();
                    }

                    // Save card
                    if (!$cardForm->addCard()) {
                        $header = Yii::t('app', 'Fail!');
                        $message = Yii::t('app', 'Error message: {error}', ['error' => implode(', ', $cardForm->getFirstErrors())]);

                        return $this->render('result', [
                            'header' => $header,
                            'message' => $message,
                        ]);
                    }

                    // Reload account
                    $account = PaymentAccount::get($model->customer_id, ConfigHelper::get('payment_account_id'));

                    // We need set payment method token for model
                    $model->payment_method_nonce = null;
                    $model->payment_method_token = $account->getPaymentToken();
                }
            } else {
                $paymentIsPossible = false;
            }

            if ($paymentIsPossible) {
                if ($model->create()) {
                    $header = Yii::t('app', 'Success!');
                    $message = Yii::t('app', 'Your transaction has been successfully processed.');
                } else {
                    $header = Yii::t('app', 'Transaction Failed');
                    $message = Yii::t('app', 'Error message: {error}', ['error' => implode(', ', $model->getFirstErrors())]);
                }

                return $this->render('result', [
                    'header' => $header,
                    'message' => $message,
                ]);
            }
        }
        $partnerId = $this->getUserPartner();

        return $this->render('index', [
            'token' => $cardForm->getToken(),
            'threeDSecureCheck' => ConfigHelper::get('threeDSecure'),
            'amount' => ($model->amount) ? $model->amount : 0,
            'fixedAmount' => false,
            'minimumAmount' => $model->getMinimumAmount(),
            'isFee' => $model->isFee(),
            'fee' => ($model->isFee()) ? $model->getFee() : 0,
            'feeAmount' => $model->isFee() ? $model->getFeeAmount() : 0,
            'currencySymbol' => $model->getCurrencySymbol(),
            'currency' => ConfigHelper::get('currency'),
            'cardAlreadyAdded' => $account->isCardAlreadyAdded,
            'cardHolderName' => ($account->isCardAlreadyAdded) ? $account->getCardHolderName() : '',
            'lastFourDigit' => $account->getCardLastFourDigit(),
            'expirationDate' => $account->getCardExpirationDate(),
            'direct' => false,
            'paymentType' => Yii::t('app', 'Pay deposit'),
            'customer' => Yii::$app->user->identity,
            'environment' => ConfigHelper::get('environment'),
            'googleMerchantId' => ConfigHelper::get('google_merchant_id', $partnerId),
            'buttonPayPal' => ConfigHelper::get('pay_by_paypal_enabled', $partnerId),
            'buttonGooglePay' => ConfigHelper::get('pay_by_google_pay_enabled', $partnerId),
        ]);
    }

    public function actionPayInvoice($item_id)
    {
        ValidateConfig::checkBrainTree(Yii::$app->user->identity);

        $model = new PayInvoiceForm();
        $model->customer_id = Yii::$app->getUser()->getId();

        // Find invoice
        $invoice = BaseInvoice::findByNumber($item_id);
        if ($invoice === null or $invoice->customer_id !== $model->customer_id) {
            throw new InvalidParamException('Invalid invoice!');
        }

        $model->invoice = $invoice;

        if ($invoice->status !== BaseInvoice::STATUS_NOT_PAID) {
            $header = Yii::t('app', 'Ok!');
            $message = Yii::t('app', 'Invoice already paid!');
            return $this->render('result', [
                'header' => $header,
                'message' => $message
            ]);
        }

        $account = PaymentAccount::get($model->customer_id, ConfigHelper::get('payment_account_id'));

        $cardForm = new BraintreeCustomerForm();
        $cardForm->customer_id = $model->customer_id;

        if (($post = Yii::$app->getRequest()->post()) !== []) {
            $paymentIsPossible = true;
            if (!empty($post['pay_from_saved_card']) && $post['pay_from_saved_card'] === 'yes') {
                // Pay from saved card
                $model->payment_method_token = $account->getPaymentToken();
            } elseif (!empty($post['payment_method_nonce'])) {
                // Pay from new card
                $model->payment_method_nonce = $post['payment_method_nonce'];

                // Check if need save card
                if (!empty($post['save_card']) && $post['save_card'] === 'on') {
                    $cardForm->customer_id = $model->customer_id;
                    $cardForm->payment_method_nonce = $model->payment_method_nonce;
                    $cardForm->cardholder_name = trim($post['cardholder_name']);

                    // Check if card is already added
                    if ($account->isCardAlreadyAdded) {
                        $cardForm->removeCard();
                    }

                    // Save card
                    if (!$cardForm->addCard()) {
                        $header = Yii::t('app', 'Fail!');
                        $message = Yii::t('app', 'Error message: {error}', ['error' => implode(', ', $cardForm->getFirstErrors())]);

                        return $this->render('result', [
                            'header' => $header,
                            'message' => $message,
                        ]);
                    }

                    // Reload account
                    $account = PaymentAccount::get($model->customer_id, ConfigHelper::get('payment_account_id'));

                    // We need set payment method token for model
                    $model->payment_method_nonce = null;
                    $model->payment_method_token = $account->getPaymentToken();
                }
            } else {
                $paymentIsPossible = false;
            }

            if ($paymentIsPossible) {
                // Make sale
                if ($model->create()) {
                    $header = Yii::t('app', 'Success!');
                    $message = Yii::t('app', 'Your transaction has been successfully processed.');
                } else {
                    $header = Yii::t('app', 'Transaction Failed');
                    $message = Yii::t('app', 'Error message: {error}', ['error' => implode(', ', $model->getFirstErrors())]);
                }

                return $this->render('result', [
                    'header' => $header,
                    'message' => $message,
                ]);
            }
        }
        $partnerId = $this->getUserPartner();

        return $this->render('index', [
            'token' => $cardForm->getToken(),
            'threeDSecureCheck' => ConfigHelper::get('threeDSecure'),
            'amount' => $model->amount,
            'fixedAmount' => true,
            'minimumAmount' => 0,
            'isFee' => $model->isFee(),
            'fee' => ($model->isFee()) ? $model->getFee() : 0,
            'feeAmount' => $model->isFee() ? $model->getFeeAmount() : 0,
            'currencySymbol' => $model->getCurrencySymbol(),
            'currency' => ConfigHelper::get('currency'),
            'cardAlreadyAdded' => $account->isCardAlreadyAdded,
            'cardHolderName' => ($account->isCardAlreadyAdded) ? $account->getCardHolderName() : '',
            'lastFourDigit' => $account->getCardLastFourDigit(),
            'expirationDate' => $account->getCardExpirationDate(),
            'direct' => false,
            'payNumber' => Yii::t('app', 'INVOICE #{id}', ['id' => $item_id]),
            'paymentType' => Yii::t('app', 'Invoice amount'),
            'customer' => Yii::$app->user->identity,
            'environment' => ConfigHelper::get('environment'),
            'googleMerchantId' => ConfigHelper::get('google_merchant_id', $partnerId),
            'buttonPayPal' => ConfigHelper::get('pay_by_paypal_enabled', $partnerId),
            'buttonGooglePay' => ConfigHelper::get('pay_by_google_pay_enabled', $partnerId),
        ]);
    }

    public function actionPayRequest($item_id)
    {
        ValidateConfig::checkBrainTree(Yii::$app->user->identity);

        $model = new PayRequestForm();
        $model->customer_id = Yii::$app->getUser()->getId();

        // Find request
        $request = BaseProformaInvoice::findByNumber($item_id);
        if ($request === null or $request->customer_id !== $model->customer_id) {
            throw new InvalidParamException(Yii::t('app', 'Invalid request!'));
        }

        $model->setInvoice($request);

        if ($request->status !== BaseProformaInvoice::STATUS_NOT_PAID) {
            $header = Yii::t('app', 'Ok!');
            $message = Yii::t('app', 'Request already paid!');
            return $this->render('result', [
                'header' => $header,
                'message' => $message
            ]);
        }

        $account = PaymentAccount::get($model->customer_id, ConfigHelper::get('payment_account_id'));
        $cardForm = new BraintreeCustomerForm();
        $cardForm->customer_id = $model->customer_id;

        if (($post = Yii::$app->getRequest()->post()) !== []) {
            $paymentIsPossible = true;
            if (!empty($post['pay_from_saved_card']) && $post['pay_from_saved_card'] === 'yes') {
                // Pay from saved card
                $model->payment_method_token = $account->getPaymentToken();
            } elseif (!empty($post['payment_method_nonce'])) {
                // Pay from new card
                $model->payment_method_nonce = $post['payment_method_nonce'];

                // Check if need save card
                if (!empty($post['save_card']) && $post['save_card'] === 'on') {
                    $cardForm->payment_method_nonce = $model->payment_method_nonce;
                    $cardForm->cardholder_name = trim($post['cardholder_name']);

                    // Check if card is already added
                    if ($account->isCardAlreadyAdded) {
                        $cardForm->removeCard();
                    }

                    if (!$cardForm->addCard()) {
                        return $this->render('result', [
                            'header' => Yii::t('app', 'Fail!'),
                            'message' => Yii::t('app', 'Error message: {error}', ['error' => implode(', ', $cardForm->getFirstErrors())]),
                        ]);
                    }

                    // Reload account
                    $account = PaymentAccount::get($model->customer_id, ConfigHelper::get('payment_account_id'));

                    // We need set payment method token for model
                    $model->payment_method_nonce = null;
                    $model->payment_method_token = $account->getPaymentToken();
                }
            } else {
                $paymentIsPossible = false;
            }

            if ($paymentIsPossible) {
                // Make sale
                if ($model->create()) {
                    $header = Yii::t('app', 'Success!');
                    $message = Yii::t('app', 'Your transaction has been successfully processed.');
                } else {
                    $header = Yii::t('app', 'Transaction Failed');
                    $message = Yii::t('app', 'Error message: {error}', ['error' => implode(', ', $model->getFirstErrors())]);
                }

                return $this->render('result', [
                    'header' => $header,
                    'message' => $message,
                ]);
            }
        }
        $partnerId = $this->getUserPartner();

        return $this->render('index', [
            'token' => $cardForm->getToken(),
            'threeDSecureCheck' => ConfigHelper::get('threeDSecure'),
            'amount' => $model->amount,
            'fixedAmount' => true,
            'minimumAmount' => 0,
            'isFee' => $model->isFee(),
            'fee' => ($model->isFee()) ? $model->getFee() : 0,
            'feeAmount' => $model->isFee() ? $model->getFeeAmount() : 0,
            'currencySymbol' => $model->getCurrencySymbol(),
            'currency' => ConfigHelper::get('currency'),
            'cardAlreadyAdded' => $account->isCardAlreadyAdded,
            'cardHolderName' => ($account->isCardAlreadyAdded) ? $account->getCardHolderName() : '',
            'lastFourDigit' => $account->getCardLastFourDigit(),
            'expirationDate' => $account->getCardExpirationDate(),
            'direct' => false,
            'payNumber' => Yii::t('app', 'REQUEST #{id}', ['id' => $item_id]),
            'paymentType' => Yii::t('app', 'Request amount'),
            'customer' => Yii::$app->user->identity,
            'environment' => ConfigHelper::get('environment'),
            'googleMerchantId' => ConfigHelper::get('google_merchant_id', $partnerId),
            'buttonPayPal' => ConfigHelper::get('pay_by_paypal_enabled', $partnerId),
            'buttonGooglePay' => ConfigHelper::get('pay_by_google_pay_enabled', $partnerId),
        ]);
    }

    public function actionAccount()
    {
        ValidateConfig::checkBrainTree(Yii::$app->user->identity);

        $customerId = Yii::$app->getUser()->getId();
        $accountId = ConfigHelper::get('payment_account_id');

        $model = PaymentAccount::get($customerId, $accountId);

        $form = new BraintreeCustomerForm();
        $form->customer_id = $customerId;

        // Check if card is already added
        if ($model->isCardAlreadyAdded) {
            return $this->render('account-info', [
                'cardHolderName' => ($model->isCardAlreadyAdded) ? $model->getCardHolderName() : '',
                'lastFourDigit' => $model->getCardLastFourDigit(),
                'expirationDate' => $model->getCardExpirationDate(),
            ]);
        }

        if (($post = Yii::$app->getRequest()->post()) !== [] and $form->load($post, '') and !empty($form->payment_method_nonce)) {
            if ($form->addCard()) {
                $header = Yii::t('app', 'Success!');
                $message = Yii::t('app', 'Your card has been successfully added.');
            } else {
                $header = Yii::t('app', 'Fail!');
                $message = Yii::t('app', 'Error message: {error}', ['error' => implode(', ', $form->getFirstErrors())]);
            }

            return $this->render('result', [
                'header' => $header,
                'message' => $message,
                'redirectTo' => Yii::$app->request->url,
            ]);
        }

        return $this->render('account', [
            'token' => $form->getToken(),
            'threeDSecureCheck' => ConfigHelper::get('threeDSecure'),
            'customer' => Yii::$app->user->identity,
        ]);
    }

    public function actionRemoveAccount()
    {
        ValidateConfig::checkBrainTree(Yii::$app->user->identity);

        $customerId = Yii::$app->getUser()->getId();

        $form = new BraintreeCustomerForm();
        $form->customer_id = $customerId;

        if (Yii::$app->getRequest()->isPost) {
            if ($form->removeCard()) {
                $header = Yii::t('app', 'Success!');
                $message = Yii::t('app', 'Your card has been successfully removed!');
            } else {
                $header = Yii::t('app', 'Failed');
                $message = Yii::t('app', 'Error message: {error}', ['error' => implode(', ', $form->getFirstErrors())]);
            }

            return $this->render('result', [
                'header' => $header,
                'message' => $message,
                'redirectTo' => '/braintree-rb/account'
            ]);
        }

        return $this->render('account', [
            'form' => $form
        ]);
    }

    /**
     * Get partner_id for an authorized user
     * @return null|integer
     */
    private function getUserPartner()
    {
        $user = Yii::$app->user->identity;
        if ($user === null) {
            return null;
        }
        return $user->partner_id;
    }
}
