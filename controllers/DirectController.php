<?php

namespace app\controllers;

use app\components\BrainTree;
use app\components\ValidateConfig;
use app\models\BraintreeCustomerForm;
use app\models\Customer;
use app\models\PayInvoiceForm;
use app\models\PaymentAccount;
use app\models\PayRequestForm;
use splynx\helpers\ConfigHelper;
use splynx\v2\models\finance\BaseInvoice;
use splynx\v2\models\finance\BaseProformaInvoice;
use Yii;
use yii\base\UserException;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\ErrorAction;

class DirectController extends Controller
{

    public function actions()
    {
        return [
            'error' => [
                'class' => ErrorAction::class,
            ]
        ];
    }

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;

        return parent::beforeAction($action);
    }

    public function actionAccount($customer_id = null)
    {
        if (!$this->adminIsAuthorized()) {
            return $this->render('/site/error', [
                'name' => Yii::t('app', 'Connection time has expired!'),
                'message' => Yii::t('app', 'Reload page please.'),
            ]);
        }

        if ($customer_id === null) {
            throw new UserException(Yii::t('app', 'Customer id empty!'));
        }

        ValidateConfig::checkBrainTree();
        $customerId = $customer_id;
        $accountId = Yii::$app->params['payment_account_id'];

        $model = PaymentAccount::get($customerId, $accountId);

        $form = new BraintreeCustomerForm();
        $form->customer_id = $customerId;

        // Check if card is already added
        if ($model->getIsCardAlreadyAdded()) {
            return $this->render('account-info', [
                'cardHolderName' => ($model->isCardAlreadyAdded) ? $model->getCardHolderName() : '',
                'lastFourDigit' => $model->getCardLastFourDigit(),
                'expirationDate' => $model->getCardExpirationDate(),
                'customer_id' => $customerId,
            ]);
        }

        if (($post = Yii::$app->getRequest()->post()) !== [] && $form->load($post, '') && !empty($form->payment_method_nonce)) {
            if ($form->addCard($customerId)) {
                $header = Yii::t('app', 'Success!');
                $message = Yii::t('app', 'Your card has been successfully added.');
            } else {
                $header = Yii::t('app', 'Fail!');
                $message = Yii::t('app', 'Error message: {error}', ['error' => implode(', ', $form->getFirstErrors())]);
            }

            return $this->render('result', [
                'header' => $header,
                'message' => $message,
                'redirectTo' => Yii::$app->request->url,
            ]);
        }
        return $this->render('/site/account', [
            'token' => $form->getToken(),
            'threeDSecureCheck' => ConfigHelper::get('threeDSecure'),
            'customer' => (new Customer())->findById($customer_id),
        ]);
    }

    public function actionDirectPayInvoice($item_id)
    {
        if (empty($item_id)) {
            throw new UserException(Yii::t('app', 'No invoice number!'));
        }

        // Find invoice
        $invoice = BaseInvoice::findByNumber($item_id);
        if (empty($invoice)) {
            throw new UserException(Yii::t('app', 'Invalid invoice number!'));
        }

        return $this->processInvoice($invoice);
    }

    public function actionDirectPayRequest($item_id)
    {
        if (empty($item_id)) {
            throw new UserException(Yii::t('app', 'No request number!'));
        }

        // Find invoice
        $invoice = BaseProformaInvoice::findByNumber($item_id);
        if (empty($invoice)) {
            throw new UserException(Yii::t('app', 'Invalid request number!'));
        }

        return $this->processInvoice($invoice);
    }

    public function actionDirectPayInvoiceById($item_id)
    {
        if (empty($item_id)) {
            throw new UserException(Yii::t('app', 'No invoice id!'));
        }

        // Find invoice
        $invoice = (new BaseInvoice())->findById($item_id);
        if (empty($item_id)) {
            throw new UserException(Yii::t('app', 'Invalid invoice id!'));
        }

        return $this->processInvoice($invoice);
    }

    public function actionDirectPayRequestById($item_id)
    {
        if (empty($item_id)) {
            throw new UserException(Yii::t('app', 'No request id!'));
        }

        // Find invoice
        $invoice = (new BaseProformaInvoice())->findById($item_id);
        if (empty($invoice)) {
            throw new UserException(Yii::t('app', 'Invalid request id!'));
        }

        return $this->processInvoice($invoice);
    }

    private function processInvoice($invoice)
    {
        ValidateConfig::checkBrainTree();

        if ($invoice instanceof BaseProformaInvoice) {
            $isProforma = true;
            $title = Yii::t('app', 'proforma invoice');
            $statusNotPaid = BaseProformaInvoice::STATUS_NOT_PAID;
        } elseif ($invoice instanceof BaseInvoice) {
            $isProforma = false;
            $title = Yii::t('app', 'invoice');
            $statusNotPaid = BaseInvoice::STATUS_NOT_PAID;
        } else {
            throw new UserException(Yii::t('app', 'Invalid invoice type!'));
        }

        if ($invoice->status !== $statusNotPaid) {
            $header = Yii::t('app', "Ok!");
            $message = Yii::t('app', "{title} already paid!", ['title' => $title]);

            return $this->render('result', [
                'header' => $header,
                'message' => $message
            ]);
        }

        if (!$isProforma) {
            $model = new PayInvoiceForm();
        } else {
            $model = new PayRequestForm();
        }

        $model->invoice = $invoice;

        BrainTree::getBraintreeComponent()->setCustomer($model->customer_id);

        // Reload account
        $account = PaymentAccount::get($model->customer_id, ConfigHelper::get('payment_account_id'));

        $cardForm = new BraintreeCustomerForm();
        $cardForm->customer_id = $model->customer_id;

        if (($post = Yii::$app->getRequest()->post()) !== []) {
            $paymentIsPossible = true;
            if (!empty($post['pay_from_saved_card']) && $post['pay_from_saved_card'] === 'yes') {
                // Pay from saved card
                $model->payment_method_token = $account->getPaymentToken();
            } elseif (!empty($post['payment_method_nonce'])) {
                // Pay from new card
                $model->payment_method_nonce = $post['payment_method_nonce'];

                // Check if need save card
                if (!empty($post['save_card']) && $post['save_card'] === 'on') {
                    $cardForm->customer_id = $model->customer_id;
                    $cardForm->payment_method_nonce = $model->payment_method_nonce;
                    $cardForm->cardholder_name = trim($post['cardholder_name']);

                    // Check if card is already added
                    if ($account->isCardAlreadyAdded) {
                        $cardForm->removeCard();
                    }

                    // Save card
                    if (!$cardForm->addCard()) {
                        $header = Yii::t('app', 'Fail!');
                        $message = Yii::t('app', 'Error message: {error}', ['error' => implode(', ', $cardForm->getFirstErrors())]);

                        return $this->render('result', [
                            'header' => $header,
                            'message' => $message,
                        ]);
                    }

                    // Reload account
                    $account = PaymentAccount::get($model->customer_id, ConfigHelper::get('payment_account_id'));

                    // We need set payment method token for model
                    $model->payment_method_nonce = null;
                    $model->payment_method_token = $account->getPaymentToken();
                }
            } else {
                $paymentIsPossible = false;
            }

            if ($paymentIsPossible) {
                // Make sale
                if ($model->create()) {
                    $header = Yii::t('app', 'Success!');
                    $message = Yii::t('app', 'Your transaction has been successfully processed.');
                } else {
                    $header = Yii::t('app', 'Transaction Failed');
                    $message = Yii::t('app', 'Error message: {error}', ['error' => implode(', ', $model->getFirstErrors())]);
                }

                return $this->render('result', [
                    'header' => $header,
                    'message' => $message,
                ]);
            }
        }

        return $this->render('/site/index', [
            'token' => $cardForm->getToken(),
            'threeDSecureCheck' => ConfigHelper::get('threeDSecure'),
            'amount' => $model->amount,
            'fixedAmount' => true,
            'minimumAmount' => 0,
            'isFee' => $model->isFee(),
            'fee' => ($model->isFee()) ? $model->getFee() : 0,
            'feeAmount' => $model->isFee() ? $model->getFeeAmount() : 0,
            'currencySymbol' => $model->getCurrencySymbol(),
            'currency' => ConfigHelper::get('currency'),
            'cardAlreadyAdded' => $account->isCardAlreadyAdded,
            'cardHolderName' => ($account->isCardAlreadyAdded) ? $account->getCardHolderName() : '',
            'lastFourDigit' => $account->getCardLastFourDigit(),
            'expirationDate' => $account->getCardExpirationDate(),
            'direct' => true,
            'payNumber' => ((!$isProforma) ? Yii::t('app', 'INVOICE') : Yii::t('app', 'REQUEST')) . ' #' . $invoice->number,
            'paymentType' => (!$isProforma) ? Yii::t('app', 'Invoice amount') : Yii::t('app', 'Request amount'),
            'customer' => BrainTree::getBraintreeComponent()->getCustomer(),
            'environment' => ConfigHelper::get('environment'),
            'googleMerchantId' => ConfigHelper::get('google_merchant_id'),
            'buttonPayPal' => ConfigHelper::get('pay_by_paypal_enabled'),
            'buttonGooglePay' => ConfigHelper::get('pay_by_google_pay_enabled'),
        ]);
    }

    public function actionRemoveAccount($customer_id = null)
    {
        if (!$this->adminIsAuthorized()) {
            return $this->render('/site/error', [
                'name' => Yii::t('app', 'Connection time has expired!'),
                'message' => Yii::t('app', 'Reload page please.'),
            ]);
        }

        if ($customer_id === null) {
            throw new UserException(Yii::t('app', 'Customer id empty!'));
        }

        $form = new BraintreeCustomerForm();
        $form->customer_id = $customer_id;

        if (Yii::$app->getRequest()->isPost) {
            if ($form->removeCard()) {
                $header = Yii::t('app', 'Success!');
                $message = Yii::t('app', 'Your card has been successfully removed!');
            } else {
                $header = Yii::t('app', 'Failed');
                $message = Yii::t('app', 'Error message: {error}', ['error' => implode(', ', $form->getFirstErrors())]);
            }

            return $this->render('result', [
                'header' => $header,
                'message' => $message,
                'redirectTo' => '/braintree-rb/direct/account?customer_id=' . $customer_id
            ]);
        }

        return $this->redirect(Url::to(['/direct/account', 'customer_id' => $customer_id]));
    }

    private function adminIsAuthorized()
    {
        return !empty(Yii::$app->getSession()->get('splynx_admin_id', null));
    }
}
