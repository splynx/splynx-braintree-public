/**
 * @var {string} _token
 * @var {number} _amount
 * @var {boolean} _fixedAmount
 * @var {number} _minimumAmount
 * @var {boolean} _threeDSecureCheck
 * @var {boolean} _isFee
 * @var {number} _fee
 * @var {string} _currencySymbol
 * @var {string} _currency
 * @var {string} _phone
 * @var {string} _email
 * @var {string} _name
 * @var {string} _environment
 * @var {string} _googleMerchantId
 * @var {boolean} _other_payment_methods
 * @var {boolean} _buttonPayPal
 * @var {boolean} _buttonGooglePay
 */

function preloader(mode, parent) {
    var parentDiv = $("#" + parent);
    switch (mode) {
        case 'show':
            parentDiv.addClass('loading');
            break;
        case 'hide':
            parentDiv.removeClass('loading');
    }
}


if (typeof _token != 'undefined') {
    let cardSaveButton = $("#card_save_button");
    let invalidFeedbackCommon = $('#invalid-feedback-common-card');
    let cardHolderName = $("#cardholder_name");
    let cardHolderNameNew = $('#cardholder_name_new');
    let cardTypeImage = $('#card_type_image');
    let paymentMethodNonce = $('#payment_method_nonce');
    let checkoutForm = $('#checkout-form');
    let checkoutFormAmount = $('#checkout_form_amount');
    let feeAmount = (_isFee) ? $('#fee_amount') : false;
    let totalAmount = (_isFee) ? $('#total_amount') : false;
    let alreadySetCardPayButton = $('#already_set_card_pay_button');
    let preloaderBlock = 'parentBlock';

    function calculateFee(value) {
        value = value / 100 * _fee;
        return Math.round(value * 100) / 100;
    }

    function calculateTotalAmount(value) {
        return ((parseFloat(value) + calculateFee(value))).toFixed(2);
    }

    preloader('show', preloaderBlock);
    braintree.client.create({
        authorization: _token,
    }, function (error, clientInstance) {
        if (error) {
            console.error(error);
            preloader('hide', preloaderBlock);
            return;
        }

        if (_threeDSecureCheck) {
            braintree.threeDSecure.create({
                version: 2, // Will use 3DS 2 whenever possible
                client: clientInstance
            }, function (threeDSecureErr, threeDSecureInstance) {
                if (threeDSecureErr) {
                    console.log('****************************** error in 3D Secure component creation');
                    console.log(threeDSecureErr);
                    // Handle error in 3D Secure component creation
                    return;
                }

                threeDSecureInstance.on('lookup-complete', function (data, next) {
                    next();
                });

                threeDSecure = threeDSecureInstance;
            });
        }

        braintree.hostedFields.create({
            client: clientInstance,
            styles: {
                'input': 'braintree-input-class',
                'input.number': {
                    'font-size': '20px',
                    'width': '100%',
                    'height': '23px',
                    'color': '#fff',
                    'font-family': 'Helvetica, Arial, sans-serif',
                    'background': 'transparent',
                    'cursor': 'pointer'
                },
                'input.expirationDate': {
                    'font-size': '20px',
                    'color': '#fff',
                    'font-family': 'Helvetica, Arial, sans-serif',
                    'background': 'transparent',
                    'cursor': 'pointer'
                },
                'input::-webkit-input-placeholder': {
                    'color': '#C7C7C7',
                    'font-style': 'italic',
                },
                'input:-moz-placeholder': {
                    'color': '#C7C7C7',
                    'font-style': 'italic',
                },
                'input:-ms-input-placeholder': {
                    'color': '#C7C7C7',
                    'font-style': 'italic',
                },
            },
            fields: {
                number: {
                    selector: '#card-number',
                    placeholder: '1111 1111 1111 1111',
                },
                expirationDate: {
                    selector: '#expiration-date',
                    placeholder: '12/23',
                },
            }
        }, function (error, hostedFieldsInstance) {
            if (error) {
                console.log('************************************************ braintree.hostedFields.create error');
                console.log(error);
                preloader('hide', preloaderBlock);
                return;
            }

            if (!_fixedAmount) {

                var paymentForm = $('#payment-form');
                var checkFormAmount = $('#checkout_form_amount');
                var noFixedAmount = $('#no_fixed_amount');

                function addCurrencySymbol(amountInput) {
                    $(amountInput).val(_currencySymbol + $(amountInput).val().replace(/^\D+/g, ''));
                }

                function validateAmount() {
                    let value = parseFloat($(noFixedAmount).val().replace(_currencySymbol, '').replace(',', '.'));
                    invalidFeedbackCommon = $('#invalid-feedback-common-pay');
                    if (value === undefined || value == null || value === "" || isNaN(value) || value == 0) {
                        validationMessage(noFixedAmount.id, 'Amount must be no less than 0.01.', 'pay-button');
                        return false;
                    } else {
                        if (value < _minimumAmount) {
                            validationMessage(noFixedAmount.id, 'Minimum amount ' + _minimumAmount, 'pay-button');
                            return false;
                        }
                    }
                    validationMessage(this.id, '', 'pay-button');
                    return value;

                }

                paymentForm.submit(function () {
                    if (validateAmount()) {
                        preloader('show', 'parentBlock');
                        noFixedAmount.val(noFixedAmount.val().replace(_currencySymbol, ''));
                        return true;
                    }
                    preloader('hide', preloaderBlock);
                    return false;
                });

                noFixedAmount.change(function () {
                    let value = validateAmount();
                    if (value) {
                        if ($(this).val().indexOf(_currencySymbol) < 0) {
                            $(this).val(_currencySymbol + $(this).val().replace(/^\D+/g, ''));
                        }

                        checkFormAmount.val(value);

                        if (_isFee) {

                            feeAmount.html(_currencySymbol + calculateFee(value));
                            totalAmount.html(_currencySymbol + calculateTotalAmount(value));
                        }

                    }
                });

                noFixedAmount.keypress(function (eventObject) {
                    let sym = String.fromCharCode(eventObject.which);
                    let value = $(eventObject.target).val();
                    if (!sym.match(/[0-9.,]/)
                        || ((sym === '.' || sym === ',')
                            &&
                            (value.indexOf('.') >= 0 || value.indexOf(',') >= 0))
                    ) {
                        return false;
                    }
                    addCurrencySymbol(this);
                    return true;
                });

                function totalPaymentAmount() {
                    let amount = validateAmount();
                    return calculateTotalAmount(amount);
                }
            } else {
                function totalPaymentAmount() {
                    return calculateTotalAmount(_amount);
                }
            }

            function validationMessage(input_id, validationMessage, buttonsSubmitClass) {
                if (validationMessage !== '') {
                    let errMsg = '<div id="' + input_id + '_error" class="invalid-feedback-message">' + validationMessage + '</div>';
                    if ($('#' + input_id + '_error').length > 0) {
                        $('#' + input_id + '_error').html(errMsg);
                    } else {
                        invalidFeedbackCommon.append(errMsg);
                    }
                    if (buttonsSubmitClass !== undefined) {
                        $('.' + buttonsSubmitClass).attr('disabled', true).addClass('disabled-button');
                    }
                    return;
                }
                $('#' + input_id + '_error').remove();
                if (buttonsSubmitClass !== undefined) {
                    $('.' + buttonsSubmitClass).attr('disabled', false).removeClass('disabled-button');
                }
            }

            function cardFormSubmit(payload) {

                paymentMethodNonce.val(payload.nonce);
                cardHolderNameNew.val(cardHolderName.val());

                if (!_fixedAmount) {
                    checkoutFormAmount.val(noFixedAmount.val().replace(_currencySymbol, ''));
                }
                cardSaveButton.prop('disabled', true);

                checkoutForm.submit();
            }

            hostedFieldsInstance.on('empty', function (event) {
                let field = event.fields[event.emittedBy];
                invalidFeedbackCommon = $('#invalid-feedback-common-card');
                switch (field.container.id) {
                    case 'card-number':
                        validationMessage(field.container.id, 'Please fill out a card number.', 'save-card-button');
                        break;
                    case 'expiration-date':
                        validationMessage(field.container.id, 'Please fill out an expiration date.', 'save-card-button');
                        break;
                }
            });

            hostedFieldsInstance.on('cardTypeChange', function (event) {
                let image_name = 'img/unknown.svg';
                invalidFeedbackCommon = $('#invalid-feedback-common-card');
                if (event.cards.length === 1) {
                    validationMessage('card-number', '', 'save-card-button');
                    switch (event.cards[0].type) {
                        case 'visa':
                            image_name = 'img/visa.svg';
                            break;
                        case 'maestro':
                            image_name = 'img/maestro.svg';
                            break;
                        case 'master-card':
                            image_name = 'img/mastercard.svg';
                            break;
                        case 'american-express':
                            image_name = 'img/ae.svg';
                            break;
                        case 'diners-club':
                            image_name = 'img/dinersclub.svg';
                            break;
                        case 'discover':
                            image_name = 'img/discover.svg';
                            break;
                        case 'jcb':
                            image_name = 'img/jcb.svg';
                            break;
                        case 'unionpay':
                        case 'maestro':
                        case 'elo':
                        case 'mir':
                        case 'hiper':
                        case 'hipercard':
                    }
                } else {
                    var field = event.fields[event.emittedBy];
                    if (!field.isValid && !field.isPotentiallyValid) {
                        validationMessage('card-number', 'Type of card not yet known.', 'save-card-button');
                    }
                }
                cardTypeImage.attr({
                    'src': image_name,
                    'style': ''
                });
            });

            hostedFieldsInstance.on('validityChange', function (event) {
                let field = event.fields[event.emittedBy];
                invalidFeedbackCommon = $('#invalid-feedback-common-card');
                validationMessage('card_registration', '');

                if (field.isValid || field.isPotentiallyValid) {
                    validationMessage(field.container.id, '', 'save-card-button');
                } else {
                    switch (field.container.id) {
                        case 'card-number':
                            validationMessage(field.container.id, 'Card number is not valid.', 'save-card-button');
                            break;
                        case 'expiration-date':
                            validationMessage(field.container.id, 'Expiration date  is not valid.', 'save-card-button');
                            break;
                    }

                }
            });

            cardHolderName.keypress(function (eventObject) {
                if (!String.fromCharCode(eventObject.which).match(/[a-zA-Zа-яА-я ]/) || $(eventObject.target).val().length > 50) {
                    return false;
                }
                return true;
            });

            cardSaveButton.on('click', function (event) {
                event.preventDefault();
                event.stopPropagation();
                event.stopImmediatePropagation();
                if (!_fixedAmount && !validateAmount()) {
                    return false;
                }

                invalidFeedbackCommon = $('#invalid-feedback-common-card');

                preloader('show', preloaderBlock);

                hostedFieldsInstance.tokenize({
                    cardholderName: cardHolderName.val()
                }, function (tokenizeErr, payload) {
                    if (tokenizeErr) {
                        let errMsg = 'Failed to process data.';

                        switch (tokenizeErr.code) {
                            case 'HOSTED_FIELDS_FIELDS_INVALID':
                                tokenizeErr.details.invalidFieldKeys.forEach(function (key) {
                                    switch (key) {
                                        case 'number':
                                            validationMessage('card-number', 'Card number is not valid.', 'save-card-button');
                                            break;
                                        case 'expirationDate':
                                            validationMessage('expiration-date', 'Expiration date  is not valid.', 'save-card-button');
                                            break;
                                    }
                                });
                                break;
                            case 'HOSTED_FIELDS_FIELDS_EMPTY':
                                validationMessage('card_registration', 'All fields are empty.');
                                break;
                            case 'HOSTED_FIELDS_TOKENIZATION_FAIL_ON_DUPLICATE':
                                validationMessage('card_registration', 'This payment method already exists in your vault.');
                                break;
                            case 'HOSTED_FIELDS_FAILED_TOKENIZATION':
                                validationMessage('card_registration', 'Tokenization failed server side. Is the card valid?');
                                break;
                            case 'HOSTED_FIELDS_TOKENIZATION_NETWORK_ERROR':
                                validationMessage('card_registration', 'Network error occurred when tokenizing.');
                                break;
                            default:
                                validationMessage('card_registration', errMsg);
                        }

                        preloader('hide', preloaderBlock);
                        /*
                        setTimeout(function () {
                            validationMessage('card_registration', '');
                        }, 3000);
                        */
                        return;
                    }

                    if (_threeDSecureCheck) {
                        var realAmount;
                        if (_fixedAmount) {
                            realAmount = _amount;
                        } else {
                            checkFormAmount.val(noFixedAmount.val().replace(_currencySymbol, ''));
                            realAmount = checkFormAmount.val();
                        }

                        threeDSecure.verifyCard({
                            challengeRequested: true,
                            nonce: payload.nonce,
                            amount: realAmount,
                            bin: payload.details.bin,
                            email: _email,
                            billingAddress: {
                                givenName: _name,
                                phoneNumber: _phone
                            }
                        }, function (error, payload) {
                            if (error) {
                                preloader('hide', preloaderBlock);
                                alert(error.message);
                                return;
                            }

                            if (payload.liabilityShifted) {
                                cardFormSubmit(payload)
                            } else {
                                preloader('hide', preloaderBlock);
                                alert('This card was ineligible for 3D Secure. Please, try another one.');
                            }
                        });

                    } else {
                        cardFormSubmit(payload);
                    }
                });
            });

            alreadySetCardPayButton.on('click', function (event) {
                preloader('show', 'parentBlock');
            });

            if (_other_payment_methods && (_buttonPayPal || _buttonGooglePay)) {
                var paypalButtonCheck = false;
                var googlePayButtonCheck = false;

                if (_buttonPayPal) {
                    preloader('show', preloaderBlock);
                    // PayPal
                    braintree.paypalCheckout.create({
                        client: clientInstance
                    }, function (paypalCheckoutErr, paypalCheckoutInstance) {
                        paypalCheckoutInstance.loadPayPalSDK({
                            currency: _currency,
                            intent: 'capture'
                        }, function () {
                            var paypalButton = paypal.Buttons({
                                fundingSource: paypal.FUNDING.PAYPAL,
                                createOrder: function (data, actions) {
                                    return paypalCheckoutInstance.createPayment({
                                        flow: 'checkout',
                                        amount: totalPaymentAmount(),
                                        currency: _currency,
                                        intent: 'capture',
                                    });
                                },
                                onApprove: function (data, actions) {
                                    return paypalCheckoutInstance.tokenizePayment(data, function (error, payload) {
                                        paymentMethodNonce.val(payload.nonce);
                                        checkoutFormAmount.val(totalPaymentAmount());
                                        checkoutForm.submit();
                                        preloader('show', 'parentBlock');
                                    });
                                }
                            });
                            if (paypalButton.isEligible()) {
                                paypalButton.render('#paypal_button');
                            }
                            paypalButtonCheck = true;
                            disablePreloaderAfterLoadingButtons();
                        });
                    });
                } else {
                    $('#paypal_button').remove();
                    paypalButtonCheck = true;
                    disablePreloaderAfterLoadingButtons();
                }

                if (_buttonGooglePay) {
                    // Google Pay
                    var googlePayClient = new google.payments.api.PaymentsClient({
                        environment: (_environment == 'production') ? 'PRODUCTION' : 'TEST',
                    });

                    braintree.googlePayment.create({
                        client: clientInstance,
                        googlePayVersion: 2,
                        googleMerchantId: _googleMerchantId
                    }, function (googlePaymentErr, googlePaymentInstance) {
                        googlePayClient.isReadyToPay({
                            apiVersion: 2,
                            apiVersionMinor: 0,
                            allowedPaymentMethods: googlePaymentInstance.createPaymentDataRequest().allowedPaymentMethods,
                            existingPaymentMethodRequired: true
                        }).then(function (response) {
                            if (response.result) {
                                var googlePayButton = googlePayClient.createButton({
                                    buttonColor: 'black',
                                    buttonType: 'plain',
                                    buttonSizeMode: 'fill',
                                    onClick: () => {
                                    }
                                });
                                document.getElementById('google_pay_button').appendChild(googlePayButton);
                                googlePayButtonCheck = true;
                                disablePreloaderAfterLoadingButtons();

                                googlePayButton.addEventListener('click', function (event) {
                                    event.preventDefault();
                                    googlePayment(googlePayClient, googlePaymentInstance);
                                });
                            }
                        }).catch(function (error) {
                            console.log('braintree.googlePayClient.isReadyToPay error');
                            console.error(error);
                            googlePayButtonCheck = true;
                            disablePreloaderAfterLoadingButtons();
                        });
                    });
                } else {
                    $('#google_pay_button').remove();
                    googlePayButtonCheck = true;
                    disablePreloaderAfterLoadingButtons();
                }

                function disablePreloaderAfterLoadingButtons() {
                    if (!$("#" + preloaderBlock).hasClass('loading')) {
                        return;
                    }

                    if (paypalButtonCheck == true && googlePayButtonCheck == true) {
                        preloader('hide', preloaderBlock);
                    }
                }

                function googlePayment(googlePayClient, googlePaymentInstance) {
                    preloader('show', 'parentBlock');
                    var paymentDataRequest = googlePaymentInstance.createPaymentDataRequest({
                        transactionInfo: {
                            currencyCode: _currency,
                            totalPriceStatus: 'FINAL',
                            totalPrice: '' + totalPaymentAmount() + ''
                        }
                    });
                    googlePayClient.loadPaymentData(paymentDataRequest).then(function (paymentData) {
                        googlePaymentInstance.parseResponse(paymentData, function (error, payload) {
                            if (error) {
                                console.log('braintree.googlePaymentInstance.parseResponse error');
                                console.log(error);
                                preloader('hide', preloaderBlock);
                                return;
                            }
                            checkoutFormAmount.val(totalPaymentAmount());
                            if (_threeDSecureCheck) {
                                threeDSecure.verifyCard({
                                    challengeRequested: true,
                                    nonce: payload.nonce,
                                    amount: totalPaymentAmount(),
                                    email: _email,
                                    bin: payload.details.bin,
                                    billingAddress: {
                                        givenName: _name,
                                        phoneNumber: _phone
                                    }
                                }, function (error, payload) {
                                    if (error) {
                                        alert(error.message);
                                        return;
                                    }

                                    if (payload.liabilityShifted) {
                                        cardFormSubmit(payload);
                                        return;
                                    }
                                    preloader('hide', preloaderBlock);
                                    alert('This card was ineligible for 3D Secure. Please, try another one.');
                                });
                            } else {
                                cardFormSubmit(payload);
                            }
                            preloader('show', 'parentBlock');
                        });
                    }).catch(function (error) {
                        console.log('braintree.loadPaymentData error');
                        console.error(error);
                        preloader('hide', preloaderBlock);
                    });
                }

                //eng google
            } else {
                preloader('hide', preloaderBlock);
            }
        });
    });

} else {
    preloader('hide', preloaderBlock);
}
