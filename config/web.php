<?php

use app\components\ValidateConfig;
use yii\base\ActionEvent;
use yii\base\UserException;

return function ($params, $baseDir) {
    return [
        'components' => [
            'request' => [
                'baseUrl' => '/braintree-rb/',
            ],
            'braintree' => [
                'class' => '\app\components\BrainTree'
            ],
            'user' => [
                'class' => 'yii\web\User',
                'identityClass' => 'app\models\Customer',
                'idParam' => 'splynx_customer_id'
            ],
        ],
        'api' => [
            'version' => SplynxApi::API_VERSION_2
        ],
        'on beforeAction' => function (ActionEvent $event) {
            if ($event->action->uniqueId !== Yii::$app->errorHandler->errorAction) {
                try {
                    ValidateConfig::checkApi();
                } catch (Exception $e) {
                    echo \Yii::$app->controller->render('@app/views/site/error.twig', [
                        'result' => false,
                        'message' => $e->getMessage(),
                    ]);
                    $event->isValid = false;
                }
            }
        },

    ];
};
