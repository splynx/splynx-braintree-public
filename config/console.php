<?php
return function ($params, $baseDir) {
    return [
        'components' => [
            //Hack for set user identity in console
            'user' => [
                'class' => 'yii\web\User',
                'identityClass' => 'splynx\v2\models\customer\BaseCustomer',
                'idParam' => 'splynx_customer_id'
            ],
            'braintree' => [
                'class' => '\app\components\BrainTree'
            ],
        ],
        'api' => [
            'version' => SplynxApi::API_VERSION_2
        ]
    ];
};
