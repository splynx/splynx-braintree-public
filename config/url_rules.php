<?php

return [
    '<alias:index|pay-invoice|pay-request|account>' => 'site/<alias>',
    '<alias:direct-pay-invoice|direct-pay-invoice-by-id|direct-pay-request|direct-pay-request-by-id|direct-result>' => 'direct/<alias>'
];
