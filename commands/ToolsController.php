<?php

namespace app\commands;

use app\models\Sync;
use yii\console\Controller;

class ToolsController extends Controller
{
    public function actionInfo()
    {
        print "Addon tools scripts\n";
        print "sync-braintree-to-splynx - save BrainTree account info to Splynx\n";
        print "sync-splynx-to-braintree-cardholder-name - save cardholder name from Splynx to BrainTree account\n";
    }

    public function actionSyncBraintreeToSplynx()
    {
        $model = new Sync();
        $model->sync();
    }

    public function actionSyncSplynxToBraintreeCardholderName()
    {
        $model = new Sync();
        $model->syncCardholderNameToBrainTree();
    }
}
