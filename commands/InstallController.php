<?php

namespace app\commands;

use splynx\base\BaseInstallController;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;

class InstallController extends BaseInstallController
{

    /**
     * @inheritdoc
     */
    public function actionIndex()
    {
        parent::actionIndex();

        // Install handler
        $handlerSourceDir = static::getBaseDir() . '/handler';
        $handlerDestinationDir = static::getSplynxDir() . 'system/external_handlers/finance/charge';
        echo PHP_EOL . Yii::t('app', 'Installing handler...') . PHP_EOL;
        if (!is_writable($handlerDestinationDir)) {
            print Yii::t('app', 'Warning! Can\'t copy files!') . PHP_EOL;
            print Yii::t('app', 'You must manually copy content of dir `{handlerSourceDir}` to `{handlerDestinationDir}`', [
                    'handlerSourceDir' => $handlerSourceDir,
                    'handlerDestinationDir' => $handlerDestinationDir
                ]) . PHP_EOL;
        } else {
            print Yii::t('app', 'Coping handler\'s files...') . PHP_EOL;
            self::recurseCopy($handlerSourceDir, $handlerDestinationDir);
            print Yii::t('app', 'Handler installing done!') . PHP_EOL;
        }

        $this->configSet('payment_account_id', reset($this->_created_payment_accounts));

        //add entry point for payment account
        print Yii::t('app', 'create entry point for payment account start') . PHP_EOL;
        $point = [
            'name' => 'braintree_rb_account_for_admin',
            'place' => 'admin',
            'type' => 'action_link',
            'size' => 'extra_large',
            'model' => 'PaymentAccountData',
            'payment_account_id' => reset($this->_created_payment_accounts),
            'title' => Yii::t('app', 'Credit card via Braintree gateway'),
            'icon' => 'fa-credit-card',
            'url' => '/braintree-rb/direct/account'
        ];
        $splynxDir = static::getSplynxDir();
        $command = "{$splynxDir}system/script/addon add-or-get-entry-point";
        $properties = ArrayHelper::merge($this->getBaseEntryPointProperties(), $point);

        foreach ($properties as $key => $value) {
            if ($key === 'url' || $key === 'code') {
                /**@see  https://jira.splynx.com/browse/SAHELPER-25 */
                $testEncodedValue = urlencode(urldecode($value));
                if ($testEncodedValue !== $value) {
                    //string is NOT urlencoded and need to urlencode
                    $value = urlencode($value);
                }
            }
            $command .= " --$key=\"$value\"";
        }

        $result = exec($command);
        if (!$result) {
            $this->stdout(Yii::t('app', 'Create entry point for payment account fail') . PHP_EOL, Console::FG_RED, Console::UNDERLINE);
        }
    }

    /**
     * @inheritdoc
     */
    public function getAddOnTitle()
    {
        return "Splynx Braintree Recurring Billing Add-On";
    }

    /**
     * @inheritdoc
     */
    public function getModuleName()
    {
        return 'splynx_braintree_rb_addon';
    }

    /**
     * @inheritdoc
     */
    public function getApiPermissions()
    {
        return [
            [
                'controller' => 'api\admin\finance\BankStatements',
                'actions' => ['index', 'view', 'add'],
            ],
            [
                'controller' => 'api\admin\finance\BankStatementsRecords',
                'actions' => ['index', 'view', 'add', 'update'],
            ],
            [
                'controller' => 'api\admin\finance\Payments',
                'actions' => ['index', 'add', 'delete'],
            ],
            [
                'controller' => 'api\admin\finance\Transactions',
                'actions' => ['index', 'add', 'delete'],
            ],
            [
                'controller' => 'api\admin\finance\Invoices',
                'actions' => ['index', 'view', 'update'],
            ],
            [
                'controller' => 'api\admin\finance\Requests',
                'actions' => ['index', 'view', 'update'],
            ],
            [
                'controller' => 'api\admin\customers\Customer',
                'actions' => ['index', 'view'],
            ],
            [
                'controller' => 'api\admin\customers\CustomerPaymentAccounts',
                'actions' => ['index', 'view', 'update', 'delete'],
            ],
            [
                'controller' => 'api\admin\customers\CustomerBilling',
                'actions' => ['index', 'view', 'update'],
            ],
        ];
    }


    /**
     * @inheritdoc
     */
    public function getEntryPoints()
    {
        return [
            [
                'name' => 'braintree_rb_invoice',
                'place' => 'portal',
                'type' => 'action_link',
                'size' => 'extra_large',
                'model' => 'Invoices',
                'title' => Yii::t('app', 'Pay by card via Braintree gateway'),
                'icon' => 'fa-credit-card',
                'url' => '/braintree-rb/pay-invoice'
            ],
            [
                'name' => 'braintree_rb_request',
                'place' => 'portal',
                'type' => 'action_link',
                'size' => 'extra_large',
                'model' => 'Requests',
                'title' => Yii::t('app', 'Pay by card via Braintree gateway'),
                'icon' => 'fa-credit-card',
                'url' => '/braintree-rb/pay-request'
            ],
            [
                'name' => 'braintree_rb_account',
                'place' => 'portal',
                'type' => 'menu_link',
                'root' => 'controllers\portal\FinanceController',
                'title' => Yii::t('app', 'Credit card via Braintree gateway'),
                'icon' => 'fa-credit-card',
                'url' => '/braintree-rb/account'
            ],
            [
                'name' => 'braintree_rb_pay_on_dashboard',
                'place' => 'portal',
                'type' => 'code',
                'root' => 'controllers\portal\DashboardController',
                'code' => urlencode(file_get_contents(Yii::$app->getViewPath() . '/dashboard.twig'))
            ],
            [
                'name' => 'braintree_rb_direct_pay_on_dashboard',
                'place' => 'portal',
                'type' => 'code',
                'root' => 'controllers\portal\DashboardController',
                'code' => urlencode(file_get_contents(Yii::$app->getViewPath() . '/dashboard-direct-pay.twig')),
                'enabled' => false
            ],
            [
                'name' => 'braintree_rb_pay_admin_invoice',
                'place' => 'admin',
                'type' => 'action_link',
                'model' => 'Invoices',
                'title' => Yii::t('app', 'Pay by Braintree gateway'),
                'icon' => 'fa-credit-card',
                'url' => '/braintree-rb/direct-pay-invoice-by-id'
            ],
            [
                'name' => 'braintree_rb_pay_admin_request',
                'place' => 'admin',
                'type' => 'action_link',
                'model' => 'Requests',
                'title' => Yii::t('app', 'Pay by Braintree gateway'),
                'icon' => 'fa-credit-card',
                'url' => '/braintree-rb/direct-pay-request-by-id'
            ],

        ];
    }

    /**
     * @inheritdoc
     */
    public function getPaymentAccounts()
    {
        return [
            [
                'title' => Yii::t('app', 'BrainTree'),
                'field_1' => Yii::t('app', 'BrainTree account'),
                'field_2' => Yii::t('app', 'Payment token'),
                'field_3' => Yii::t('app', 'Card 4 last digit'),
                'field_4' => Yii::t('app', 'Cardholder name'),
                'field_5' => Yii::t('app', 'Card expiration date'),
            ]
        ];
    }

    /**
     * @param string $src path from
     * @param string $dst path to
     */
    private static function recurseCopy($src, $dst)
    {
        $dir = opendir($src);
        @mkdir($dst);
        while (false !== ($file = readdir($dir))) {
            if (($file != '.') && ($file != '..')) {
                if (is_dir($src . '/' . $file)) {
                    self::recurseCopy($src . '/' . $file, $dst . '/' . $file);
                } else {
                    copy($src . '/' . $file, $dst . '/' . $file);
                }
            }
        }
        closedir($dir);
    }
}
