<?php

namespace app\commands;

use app\components\BrainTree;
use app\models\PayInvoiceForm;
use app\models\PaymentAccount;
use app\models\PayRequestForm;
use splynx\helpers\ConfigHelper;
use splynx\v2\models\customer\BaseCustomer;
use splynx\v2\models\finance\BaseInvoice;
use splynx\v2\models\finance\BaseProformaInvoice;
use Yii;
use yii\console\Controller;

class ChargeController extends Controller
{
    public function actionChargeInvoice($id)
    {
        echo Yii::t('app', 'Work with invoice with id {id}', ['id' => $id]) . "\n";
        $model = new PayInvoiceForm();

        // Find invoice
        $invoice = (new BaseInvoice())->findById($id);
        if (empty($invoice)) {
            echo Yii::t('app', 'invoice not found!');
            return;
        }
        echo Yii::t('app', 'loaded invoice number "{number}"', ['number' => $invoice->number]) . "\n";

        $model->setInvoice($invoice);
        $model->customer_id = $invoice->customer_id;
        echo Yii::t('app', 'customer id: {id}', ['id' => $model->customer_id]) . "\n";

        //set user identity for Yii
        $customer = (new BaseCustomer)->findById($model->customer_id);
        Yii::$app->user->setIdentity($customer);
        BrainTree::getBraintreeComponent()->setCustomer($model->customer_id);

        if ($invoice->status !== BaseInvoice::STATUS_NOT_PAID) {
            if ($invoice->status === BaseInvoice::STATUS_DELETED) {
                echo Yii::t('app', 'invoice marked as deleted!');
            } elseif ($invoice->status === BaseInvoice::STATUS_PAID) {
                echo Yii::t('app', 'invoice already paid!');
            } else {
                echo Yii::t('app', 'unknown invoice status!');
            }
            return;
        }

        echo Yii::t('app', 'invoice amount: {amount}', ['amount' => $model->amount]) . "\n";
        if ($model->isFee()) {
            echo Yii::t('app', 'fee amount: {amount}', ['amount' => $model->getFeeAmount()]) . "\n";
        } else {
            echo Yii::t('app', 'no fee') . "\n";
        }
        echo Yii::t('app', 'total amount: {amount}', ['amount' => $model->getTotalAmount()]) . "\n";

        // Get payment method nonce
        $paymentAccount = PaymentAccount::get($model->customer_id, ConfigHelper::get('payment_account_id'));
        $token = $paymentAccount->getPaymentToken();
        if (empty($token)) {
            echo Yii::t('app', 'no payment token in customer account!');
            return;
        }
        $model->payment_method_token = $paymentAccount->getPaymentToken();

        if ($model->create()) {
            echo Yii::t('app', 'charged!');
        } else {
            echo Yii::t('app', 'NOT charged! Reason: {error}', ['error' => implode(', ', $model->getFirstErrors())]);
        }
    }

    public function actionChargeRequest($id)
    {
        echo Yii::t('app', 'Work with request with id {id}', ['id' => $id]) . "\n";
        $model = new PayRequestForm();

        // Find request
        $request = (new BaseProformaInvoice)->findById($id);
        if (empty($request)) {
            echo Yii::t('app', 'request not found!');
            return;
        }
        echo Yii::t('app', 'loaded request number "{number}"', ['number' => $request->number]) . "\n";

        $model->setInvoice($request);
        $model->customer_id = $request->customer_id;
        echo Yii::t('app', 'customer id: {id}', ['id' => $model->customer_id]) . "\n";

        //set user identity for Yii
        $customer = (new BaseCustomer)->findById($model->customer_id);
        Yii::$app->user->setIdentity($customer);
        BrainTree::getBraintreeComponent()->setCustomer($model->customer_id);

        if ($request->status !== BaseProformaInvoice::STATUS_NOT_PAID) {
            if ($request->status === BaseProformaInvoice::STATUS_PAID) {
                echo Yii::t('app', 'request already paid!');
            } else {
                echo Yii::t('app', 'unknown request status!');
            }
            return;
        }

        echo Yii::t('app', 'request amount: {amount}', ['amount' => $model->amount]) . "\n";
        if ($model->isFee()) {
            echo Yii::t('app', 'fee amount: {amount}', ['amount' => $model->getFeeAmount()]) . "\n";
        } else {
            echo Yii::t('app', 'no fee') . "\n";
        }
        echo Yii::t('app', 'total amount: {amount}', ['amount' => $model->getTotalAmount()]) . "\n";

        // Get payment method nonce
        $paymentAccount = PaymentAccount::get($model->customer_id, ConfigHelper::get('payment_account_id'));
        $token = $paymentAccount->getPaymentToken();
        if (empty($token)) {
            echo Yii::t('app', 'no payment token in customer account!');
            return;
        }
        $model->payment_method_token = $paymentAccount->getPaymentToken();

        if ($model->create()) {
            echo Yii::t('app', 'charged!');
        } else {
            echo Yii::t('app', 'NOT charged! Reason: {error}', ['error' => implode(', ', $model->getFirstErrors())]);
        }
    }
}
